<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Privileges extends CI_Model {

	public function register_user_account($data){
		$this->db->insert('user_account', $data);
		$account_id = $this->db->insert_id();
		return $account_id;
	}
	public function post_client($data){
		$username = $this->session->username;
		$query_1 = "SELECT id FROM user_account WHERE username = ?";
		$sql_1 = $this->db->query($query_1, $username);
		$result_1 = $sql_1->result_array();
		foreach($result_1 as $account_data){
			$account_id = $account_data['id'];
		}
		$data['account_id'] = $account_id;
		$this->db->insert('client_post', $data);
		$post_id = $this->db->insert_id();
		$ids = array(
			'account_id' => $account_id,
			'post_id' => $post_id);
		return $ids;
	}

	public function update_post_client($insert_data, $post_id){
		$this->db->where('id', $post_id);
		$this->db->update('client_post', $insert_data);
	}
	public function fetch_post_members($post_id){
		$query_1 = $this->db->query("SELECT user_profile.first_name, user_profile.last_name, post_member.id, 
		post_member.builder_id FROM user_profile, post_member WHERE user_profile.account_id = 
		post_member.builder_id AND post_member.post_id = $post_id")->result_array();
		return $query_1;
	}
	public function fetch_post_client(){
		$query_1 = $this->db->query("SELECT client_post.id, client_post.type, client_post.account_id, client_post.title, client_post.description, client_post.land_description, client_post.post_start, client_post.post_end, client_post.status, user_profile.first_name, user_profile.last_name, user_account.email FROM client_post, user_profile, user_account WHERE client_post.account_id = user_profile.account_id AND user_account.id = user_profile.account_id ORDER BY client_post.post_start DESC")->result_array();
		return $query_1;
	}
	public function fetch_post_account(){
		$email = $this->session->email;
		$query_1 = "SELECT id FROM user_account WHERE email = ?";
		$sql_1 = $this->db->query($query_1, $email)->result_array();
		foreach($sql_1 as $account_data){
			$account_id = $account_data['id'];
		}
		$query_2 = $this->db->query("SELECT client_post.id, client_post.type, client_post.account_id, client_post.title, client_post.description, client_post.land_description, client_post.post_start, client_post.post_end, client_post.status, user_profile.first_name, user_profile.last_name FROM client_post, user_profile WHERE client_post.account_id = $account_id AND user_profile.account_id = $account_id")->result_array();
		return $query_2;
	}
	public function fetch_application_account(){
		$email = $this->session->email;
		$query_1 = "SELECT id FROM user_account WHERE email = ?";
		$sql_1 = $this->db->query($query_1, $email)->result_array();
		foreach($sql_1 as $account_data){
			$account_id = $account_data['id'];
		}
		$query_2 = $this->db->query("SELECT client_post.id, client_post.type, client_post.account_id, client_post.title, client_post.description, client_post.land_description, client_post.post_start, client_post.post_end, client_post.status, user_profile.first_name, user_profile.last_name FROM client_post, user_profile, builder_post_request WHERE client_post.id = builder_post_request.post_id AND user_profile.account_id = client_post.account_id AND builder_post_request.builder_id = $account_id")->
		result_array();
		return $query_2;
	}
	public function fetch_proposal_account(){
		$email = $this->session->email;
		$query_1 = "SELECT id FROM user_account WHERE email = ?";
		$sql_1 = $this->db->query($query_1, $email)->result_array();
		foreach($sql_1 as $account_data){
			$account_id = $account_data['id'];
		}
		$query_2 = $this->db->query("SELECT client_post.id, client_post.type, client_post.account_id, client_post.title, client_post.description, client_post.land_description, client_post.post_start, client_post.post_end, client_post.status, user_profile.first_name, user_profile.last_name FROM client_post, user_profile, post_member WHERE client_post.id = post_member.post_id AND user_profile.account_id = client_post.account_id AND post_member.builder_id = $account_id")->result_array();
		return $query_2;
	}


	public function fetch_post_file($post_id){
		$query_1 = "SELECT file_name FROM post_file WHERE post_id = ?";

		$sql_1 = $this->db->query($query_1, $post_id)->result_array();
		return $sql_1;
	}
	public function delete_post_file($post_id){
		$query_1 = "DELETE FROM post_file WHERE post_id = ?";
		$this->db->query($query_1, $post_id);
	}
	public function delete_post($post_id){
		$query_1 = "DELETE FROM client_post WHERE id = ?";
		$this->db->query($query_1, $post_id);
	}
	public function fetch_post_info($id){
		$query_1 = $this->db->query("SELECT client_post.id, client_post.type, client_post.account_id, client_post.title, client_post.description, client_post.land_description, client_post.post_start, client_post.post_end, client_post.status, user_profile.first_name, user_profile.last_name, user_account.email FROM client_post, user_profile, user_account WHERE client_post.account_id = user_profile.account_id AND user_account.id = user_profile.account_id AND client_post.id = $id")->result_array();
		return $query_1;
	}
	public function cancel_application($post_id){
		$email = $this->session->email;
		$query_1 = "SELECT id FROM user_account WHERE email = ?";
		$sql_1 = $this->db->query($query_1, $email)->result_array();
		foreach($sql_1 as $account_data){
			$account_id = $account_data['id'];
		}
		$data = array($post_id, $account_id);
		$query_2 = "DELETE FROM builder_post_request WHERE post_id = ? AND builder_id = ?";
		$this->db->query($query_2, $data);
	}
	public function view_applicants($post_id){
		$query_1 = $this->db->query("SELECT user_profile.first_name, user_profile.last_name, 
		user_profile.account_id, builder_post_request.id, builder_post_request.client_id, 
		builder_post_request.builder_id, builder_post_request.post_id FROM user_profile, builder_post_request 
		WHERE user_profile.account_id = builder_post_request.builder_id AND builder_post_request.post_id = 
		$post_id")->result_array();
		return $query_1;
	}
}