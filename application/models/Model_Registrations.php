<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Registrations extends CI_Model {

	public function register_user_account($data){
		$this->db->insert('user_account', $data);
		$account_id = $this->db->insert_id();
		return $account_id;
	}
	public function register_user_profile($data){
		$this->db->insert('user_profile', $data);
	}
	public function register_user_client($data){
		$this->db->insert('user_client', $data);
	}
	public function checkUsername($username){
		$query = $this->db->where('username',$username)
							->get('user_account');

		return $query->num_rows();
	}
	public function getUserAccount(){
		return $this->db->select('id')
						->order_by('id', 'DESC')
						->limit(1)
						->get('user_account')
						->result_array();
	}
}