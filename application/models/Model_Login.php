<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, DELETE, OPTION');
header('Access-Control-Allow-Headers: *');

class Model_Login extends CI_Model {

	function loginVerify($username, $password){
		$query_1 = "SELECT * FROM user_account WHERE BINARY username = ? AND BINARY password = ?";
		$data_1 = array($username, $password);
		$sql_1 = $this->db->query($query_1,$data_1);
		$result_1 = $sql_1->result_array();
		if($result_1 != null){
			foreach($result_1 as $account_data){
				$account_id = $account_data['id'];
				$username = $account_data['username'];
				$email = $account_data['email'];
				$contact = $account_data['contact'];
				$profile_pic = $account_data['profile_pic'];
				$privilege = $account_data['privilege'];
			}
			$query_2 = "SELECT * FROM user_profile WHERE account_id = ?";
			$sql_2 = $this->db->query($query_2, $account_id);
			$result_2 = $sql_2->result_array();
			foreach($result_2 as $profile_data){
				$fname = $profile_data['first_name'];
				$lname = $profile_data['last_name'];
				$gender = $profile_data['gender'];
				$birth_date = $profile_data['birth_date'];
			}
			$full_name = $fname." ".$lname;
			if($privilege == 'Admin'){
				$session_data = array(
					'username' => $username,
					'email' => $email,
					'contact' => $contact,
					'profile_pic' => $profile_pic,
					'privilege' => $privilege,
					'full_name' => $full_name,
					'gender' => $gender,
					'birth_date' => $birth_date
				);
				$this->session->set_userdata($session_data);
			}
			if($privilege == 'Client'){
				$query_3 = "SELECT * FROM user_client WHERE account_id = ?";
				$sql_3 = $this->db->query($query_3, $account_id);
				$result_3 = $sql_3->result_array();
				foreach($result_3 as $user_client){
					$occupation = $user_client['occupation'];
					$work_address = $user_client['work_address'];
				}
				$session_data = array(
					'username' => $username,
					'email' => $email,
					'contact' => $contact,
					'profile_pic' => $profile_pic,
					'privilege' => $privilege,
					'full_name' => $full_name,
					'gender' => $gender,
					'birth_date' => $birth_date,
					'occupation' => $occupation,
					'work_address' => $work_address
				);
				$this->session->set_userdata($session_data);
			}
			if($privilege == 'Builder'){
				$query_3 = "SELECT * FROM user_builder WHERE account_id = ?";
				$sql_3 = $this->db->query($query_3, $account_id);
				$result_3 = $sql_3->result_array();
				foreach($result_3 as $user_builder){
					$degree = $user_builder['degree'];
					$firm = $user_builder['firm'];
				}
				$session_data = array(
					'username' => $username,
					'email' => $email,
					'contact' => $contact,
					'profile_pic' => $profile_pic,
					'privilege' => $privilege,
					'full_name' => $full_name,
					'gender' => $gender,
					'birth_date' => $birth_date,
					'degree' => $degree,
					'firm' => $firm
				);
				$this->session->set_userdata($session_data);
			}
			return true;
		}
		else{
			return false;
		}
	}
}