<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Update extends CI_Model {

	public function fetch_password(){
		$username = $this->session->username;
		$query_1 = "SELECT password FROM user_account WHERE username = ?";
		$sql_1 = $this->db->query($query_1, $username);
		$result_1 = $sql_1->result_array();
		foreach ($result_1 as $account_data) {
			$password = $account_data['password'];
		}
		return $password;
	}
	public function update_password($new_pass){
		$username = $this->session->username;
		$query_1 = "UPDATE user_account SET password = ? WHERE username = ?";
		$insert_data = array($new_pass,$username);
		$sql_1 = $this->db->query($query_1, $insert_data);
	}
	public function update_fullname($fname,$lname){
		$username = $this->session->username;
		$query_1 = "SELECT id FROM user_account WHERE username = ?";
		$sql_1 = $this->db->query($query_1, $username);
		$result_1 = $sql_1->result_array();
		foreach($result_1 as $account_data){
			$account_id = $account_data['id'];
		}
		$query_2 = "UPDATE user_profile SET first_name = ?, last_name = ? WHERE account_id = ?";
		$insert_data = array($fname,$lname,$account_id);
		$sql_2 = $this->db->query($query_2,$insert_data);
	}
	public function update_birthdate($bdate){
		$username = $this->session->username;
		$query_1 = "SELECT id FROM user_account WHERE username = ?";
		$sql_1 = $this->db->query($query_1, $username);
		$result_1 = $sql_1->result_array();
		foreach($result_1 as $account_data){
			$account_id = $account_data['id'];
		}
		$query_2 = "UPDATE user_profile SET birth_date = ? WHERE account_id = ?";
		$insert_data = array($bdate,$account_id);
		$sql_2 = $this->db->query($query_2,$insert_data);
	}
	public function update_email($email){
		$username = $this->session->username;
		$query_1 = "UPDATE user_account SET email = ? WHERE username = ?";
		$data_1 = array($email,$username);
		$sql_1 = $this->db->query($query_1, $data_1);
	}
	public function update_contact($contact){
		$username = $this->session->username;
		$query_1 = "UPDATE user_account SET contact = ? WHERE username = ?";
		$data_1 = array($contact,$username);
		$sql_1 = $this->db->query($query_1, $data_1);
	}
	public function update_occupation($occupation){
		$username = $this->session->username;
		$query_1 = "SELECT id FROM user_account WHERE username = ?";
		$sql_1 = $this->db->query($query_1, $username);
		$result_1 = $sql_1->result_array();
		foreach($result_1 as $user_account){
			$account_id = $user_account['id'];
		}
		$query_2 = "UPDATE user_client SET occupation = ? WHERE account_id = ?";
		$data_2  = array($occupation, $account_id);
		$sql_2 = $this->db->query($query_2, $data_2);
	}
	public function update_workaddress($work_address){
		$username = $this->session->username;
		$query_1 = "SELECT id FROM user_account WHERE username = ?";
		$sql_1 = $this->db->query($query_1, $username);
		$result_1 = $sql_1->result_array();
		foreach($result_1 as $user_account){
			$account_id = $user_account['id'];
		}
		$query_2 = "UPDATE user_client SET work_address = ? WHERE account_id = ?";
		$data_2  = array($work_address, $account_id);
		$sql_2 = $this->db->query($query_2, $data_2);
	}
}