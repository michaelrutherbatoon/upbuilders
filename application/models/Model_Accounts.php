<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Accounts extends CI_Model {

	public function select_all_admin(){
		$username = $this->session->username;
		$query_1 = "SELECT id FROM user_account WHERE username = ?";
		$sql_1 = $this->db->query($query_1, $username);
		$result_1 = $sql_1->result_array();
		foreach($result_1 as $account_data){
			$id = $account_data['id'];
		}

		$query_1 = $this->db->query("SELECT user_profile.account_id, user_profile.first_name, user_profile.last_name, user_profile.gender, user_profile.birth_date, user_account.email, user_account.contact, user_account.is_active FROM user_profile,user_account WHERE user_account.id != $id AND user_profile.account_id = user_account.id AND user_account.privilege = 'Admin'")->result_array();
		return $query_1;
	}
	public function select_all_obo(){
		$query_1 = $this->db->query("SELECT user_profile.account_id, user_profile.first_name, user_profile.last_name, user_profile.gender, user_profile.birth_date, user_account.email, user_account.contact, user_account.is_active FROM user_profile,user_account WHERE user_profile.account_id = user_account.id AND user_account.privilege = 'OBO'")->result_array();
		return $query_1;
	}
	public function select_all_client(){
		$query_1 = $this->db->query("SELECT user_profile.account_id, user_profile.first_name, user_profile.last_name, user_profile.gender, user_profile.birth_date, user_account.email, user_account.contact, user_account.is_active FROM user_profile,user_account WHERE user_profile.account_id = user_account.id AND user_account.privilege = 'Client'")->result_array();
		return $query_1;
	}
	public function select_account($id){
		$query_1 = "SELECT * FROM user_account WHERE id = ?";
		$sql_1 = $this->db->query($query_1, $id);
		$result_1 = $sql_1->result_array();
		return $result_1;
	}
	public function select_profile($id){
		$query_1 = "SELECT * FROM user_profile WHERE account_id = ?";
		$sql_1 = $this->db->query($query_1, $id);
		$result_1 = $sql_1->result_array();
		return $result_1;
	}
	public function select_client($id){

	}
	public function select_builder($id){

	}
	public function change_account_status($id){
		$query_1 = "SELECT is_active FROM user_account WHERE id = ?";
		$sql_1 = $this->db->query($query_1, $id);
		$result_1 = $sql_1->result_array();
		foreach($result_1 as $account_data){
			$status = $account_data['is_active'];
		}
		if($status == 1){
			$data_2 = array(0, $id);
		}
		elseif($status == 0){
			$data_2 = array(1, $id);
		}
		$query_2 = "UPDATE user_account SET is_active = ? WHERE id = ?";
		$sql_2 = $this->db->query($query_2, $data_2);
	}
	public function reset_account($id){
		$query_1 = "UPDATE user_account SET password = ? WHERE id = ?";
		$data_1 = array('12345', $id);
		$sql_1 = $this->db->query($query_1, $data_1);
	}
	public function check_privilege($id){
		$query_1 = "SELECT privilege FROM user_account WHERE id = ?";
		$sql_1 = $this->db->query($query_1, $id);
		$result = $sql_1->result_array();
		foreach($result as $data){
			$priv = $data['privilege'];
		}
		return $priv;
	}

}