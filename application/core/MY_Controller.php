<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function xss_check($str){
		if($this->security->xss_clean($str, TRUE) === FALSE){
			$this->form_validation->set_message('xss_check', 'The {field} field is invalid. XSS Alert!');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	public function username_check($str){
		if($this->security->xss_clean($str, TRUE) === FALSE){
			$this->form_validation->set_message('username_check', 'The {field} field is invalid. XSS Alert!');
			return FALSE;
		}
		else{
			if (preg_match('/^[a-z0-9_.\-]+$/i', $str)){
			return TRUE;
			}
			elseif($str == null){
				$this->form_validation->set_message('username_check', 'The {field} field is required');
			    return FALSE;
			}
			else{
			    $this->form_validation->set_message('username_check', 'The {field} field is invalid');
			    return FALSE;
			}
		}
	}
	public function password_check($str){
		if(isset($str)){
			if($this->security->xss_clean($str, TRUE) === FALSE){
				$this->form_validation->set_message('password_check', 'The {field} field is invalid. XSS Alert!');
				return FALSE;
			}
			else{
				if($str == null){
					$this->form_validation->set_message('password_check', 'The {field} field is required.');
					return FALSE;
				}
				elseif(strlen($str) <= 5){
					$this->form_validation->set_message('password_check', 'The {field} must have 5+ characters.');
					return FALSE;
				}
				elseif (!preg_match('/^[a-z0-9 .\-_]+$/i', $str)){
					$this->form_validation->set_message('password_check', 'The {field} is invalid.');
					return FALSE;
				}
		        else{
		            return TRUE;
		        }
			}
    	}
    	else{
			$dest = base_url();
			redirect($dest);
		}
	}
	public function oldpassword_check($str){
		if(isset($str)){
			if($this->security->xss_clean($str, TRUE) === FALSE){
				$this->form_validation->set_message('oldpassword_check', 'The {field} field is invalid. XSS Alert!');
				return FALSE;
			}
			else{
				$old_password = $this->Model_Update->fetch_password();
				if($str == null){
					$this->form_validation->set_message('oldpassword_check', 'The {field} field is required.');
				}
				elseif (!preg_match('/^[a-z0-9 .\-_]+$/i', $str)){
					$this->form_validation->set_message('oldpassword_check', 'The {field} is invalid.');
					return FALSE;
				}
		        elseif($str != $old_password){
		            $this->form_validation->set_message('oldpassword_check', 'The {field} field does not match in your 
		            current password.');
		           	return FALSE;
		        }
		        else{
		            return TRUE;
		        }
			}
			
    	}
    	else{
			$dest = base_url();
			redirect($dest);
		}
    }
	public function name_check($str){
    	if(isset($str)){
    		if($this->security->xss_clean($str, TRUE) === FALSE){
				$this->form_validation->set_message('name_check', 'The {field} field is invalid. XSS Alert!');
				return FALSE;
			}
			else{
				if (preg_match('/^[a-z0-9 .\-]+$/i', $str)){
					return TRUE;
				}
				elseif($str == null){
					$this->form_validation->set_message('name_check', 'The {field} field is required.');
				    return FALSE;
				}
				else{
				    $this->form_validation->set_message('name_check', 'The {field} field is invalid.');
				    return FALSE;
				}
			}
		}
		else{
			$dest = base_url();
			redirect($dest);
		}
	}
	public function address_check($str){
    	if(isset($str)){
    		if($this->security->xss_clean($str, TRUE) === FALSE){
				$this->form_validation->set_message('address_check', 'The {field} field is invalid. XSS Alert!');
				return FALSE;
			}
			else{
				if (preg_match('/^[a-z0-9 .#,\-]+$/i', $str)){
					return TRUE;
				}
				elseif($str == null){
					$this->form_validation->set_message('address_check', 'The {field} field is required.');
				    return FALSE;
				}
				else{
				    $this->form_validation->set_message('address_check', 'The {field} field is invalid.');
				    return FALSE;
				}
			}
		}
		else{
			$dest = base_url();
			redirect($dest);
		}
	}
	public function email_check($str){
		if(isset($str)){
			if($this->security->xss_clean($str, TRUE) === FALSE){
				$this->form_validation->set_message('email_check', 'The {field} field is invalid. XSS Alert!');
				return FALSE;
			}
			else{
				if (preg_match('/^[a-z0-9.\@]+$/i', $str)){
					return TRUE;
				}
				elseif($str == null){
					$this->form_validation->set_message('email_check', 'The {field} field is required.');
				    return FALSE;
				}
				else{
				    $this->form_validation->set_message('email_check', 'The {field} field is an invalid email address.
				    Please use letters, numbers and period sign only.');
				    return FALSE;
				}
			}
		}
		else{
			$dest = base_url();
			redirect($dest);
		}
	}
	public function date_check($str){
		if(isset($str)){
			if($this->security->xss_clean($str, TRUE) === FALSE){
				$this->form_validation->set_message('date_check', 'The {field} field is invalid. XSS Alert!');
				return FALSE;
			}
			else{
				if($str == null){
					$this->form_validation->set_message('date_check', 'The {field} field is required.');
					return FALSE;
				}
				elseif (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$str)){
					return TRUE;
				}
				else{
				    $this->form_validation->set_message('date_check', 'The {field} field is an invalid date.');
				    return FALSE;
				}
			}
			
		}
		else{
			$dest = base_url();
			redirect($dest);
		}
	}
	public function contact_check($str){
		if(isset($str)){
			if($this->security->xss_clean($str, TRUE) === FALSE){
				$this->form_validation->set_message('contact_check', 'The {field} field is invalid. XSS Alert!');
				return FALSE;
			}
			else{
				if($str == null){
					$this->form_validation->set_message('contact_check', 'The {field} field is required.');
					return FALSE;
				}
				elseif(preg_match("/^[0-9]{10}$/", $str)) {
			  		return TRUE;
				}
				else{
					$this->form_validation->set_message('contact_check', 'The {field} field is invalid.');
					return FALSE;
				}
			}
			
		}
		else{
			$dest = base_url();
			redirect($dest);
		}
	}

}
?>