
    <div class="vertical-timeline-icon navy-bg">
        <i class="fa fa-building"></i>
    </div>
    <div class="vertical-timeline-content">
        
        <?php  
        if($this->session->email == $data['email']){
        ?>
            <a href="<?php echo site_url('Modal_Bodies/post_update_form/'.
            $data['id']);?>" id="update_post" class="btn btn-sm btn-warning 
            btn-outline btn-post-option">Update</a>
            <a href="<?php echo site_url('Modal_Bodies/delete_post_confirmation/'.$data['id']); ?>" 
            class="btn btn-sm btn-danger btn-outline btn-post-option"
            id="confirm_delete_post"> Delete</a>
        <?php  
        }
        ?>
    </div>

