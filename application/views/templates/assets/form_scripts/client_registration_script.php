<script type="text/javascript">
	$( document ).ready(function() {
		$("#loader_animation").hide();
		$('form#clientRegForm').on('submit', function(){
	        var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
	        that.find('[name]').each(function(index,value){
	              var that = $(this), name = that.attr('name'), value = that.val();
	              data[name] = value;
	        });
	        $.ajax({
	            url: url,
	            type: type,
	            data: data,
	            cache: false,
	            beforeSend: function(){ $("#loader_animation").show(); },
	            success: function(response){
	            	if(response == 'true'){
	            		$("#loader_animation").hide();
	            		window.location.href='<?php echo site_url('Account/landing_page'); ?>';
	            	}
	                $("#loader_animation").hide();
	                $('#form_client_reg').html(response);              
	            },
	            error: function(){
	                console.log("UH OH! SOMETHING WENT WRONG");
	            }
	        });
	        return false;
	    });
	});
</script>