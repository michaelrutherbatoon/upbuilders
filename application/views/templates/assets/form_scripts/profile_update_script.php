<script type="text/javascript">
  $(document).ready(function(){
      $("a#update_password, a#update_fullname, a#update_birthdate, a#update_email, a#update_contact, a#update_occupation, a#update_workaddress, a#update_degree, a#update_firm")
      .click(function(event){
          event.preventDefault(); 
          var myurl = $(this).attr("href");
          var id = $(this).attr("id");
          if(id == 'update_password'){
            $('h1#label').text('Update Password');
          }
          else if(id == 'update_fullname'){
            $('h1#label').text('Update Full Name');
          }
          else if(id == 'update_birthdate'){
            $('h1#label').text('Update Birth Date');
          }
          else if(id == 'update_email'){
            $('h1#label').text('Update Email Address');
          }
          else if(id == 'update_contact'){
            $('h1#label').text('Update Contact Number');
          }
          else if(id == 'update_occupation'){
            $('h1#label').text('Update Occupation');
          }
          else if(id == 'update_workaddress'){
            $('h1#label').text('Update Work Address');
          }
          else if(id == 'update_degree'){
            $('h1#label').text('Update Degree');
          }
          else if(id == 'update_firm'){
            $('h1#label').text('Update Firm');
          }
      $.ajax({
          url: myurl,
          success: function(msg) {
            $('#form_modal').modal('show');
            $('#form_modal_content, div.modal-footer, h1#label').show();
            $('#form_modal_content').html(msg);
          }
        });
      });
  });
</script>