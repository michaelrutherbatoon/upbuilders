<script type="text/javascript">
	$("#loader_animation").hide();

    $('form#normal_form').on('submit', function(){
        var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
        that.find('[name]').each(function(index,value){
            var that = $(this), name = that.attr('name'), value = that.val();
            data[name] = value;
        });
        $.ajax({
            url: url,
            type: type,
            data: data,
            cache: false,
            beforeSend: function(){ $("#loader_animation").show(); },
            success: function(response){
            	$("#loader_animation").hide();
                $('#form_body').html(response);
                
            },
            error: function(){
                console.log("UH OH! SOMETHING WENT WRONG");
            }
        });
        return false;
    });
</script>