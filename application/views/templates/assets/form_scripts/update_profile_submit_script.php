<script type="text/javascript">
    $("#loader_animation").hide();
	$("#success_message").hide();

    $('form#update_profile_form').on('submit', function(){
        var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
        that.find('[name]').each(function(index,value){
            var that = $(this), name = that.attr('name'), value = that.val();
            data[name] = value;
        });
        $.ajax({
            url: url,
            type: type,
            data: data,
            cache: false,
            beforeSend: function(){ $("#loader_animation").show(); },
            success: function(response){
                if(response == 'password_updated'){
                    window.location.href='<?php echo site_url('UB/login'); ?>';
                }
                if(response == 'name_updated'){
                    $("#sidemenu_full_name_display").text(data.first_name+" "+data.last_name);
                    $("#loader_animation").hide();
                    $('h4.modal-title').text('NOTIFICATION');
                    $("#modal-body").html("<h2><b>FULL NAME UPDATED...</b></h2>");
                    $('td#full_name_display').text(data.first_name+" "+data.last_name);
                }
                else if(response == 'bdate_updated'){
                    $("#loader_animation").hide();
                    $('h4.modal-title').text('NOTIFICATION');
                    $("#modal-body").html("<h2><b>BIRTH DATE UPDATED...</b></h2>");
                    $('td#birth_date_display').text(data.birth_date);
                }
                else if(response == 'email_updated'){
                    $("#loader_animation").hide();
                    $('h4.modal-title').text('NOTIFICATION');
                    $("#modal-body").html("<h2><b>EMAIL ADDRESS UPDATED...</b></h2>");
                    $('td#email_display').text(data.email);
                }
                else if(response == 'contact_updated'){
                    $("#loader_animation").hide();
                    $('h4.modal-title').text('NOTIFICATION');
                    $("#modal-body").html("<h2><b>CONTACT NUMBER UPDATED...</b></h2>");
                    $('td#contact_display').text("+63"+data.contact);
                }
                else if(response == 'occupation_updated'){
                    $("#loader_animation").hide();
                    $('h4.modal-title').text('NOTIFICATION');
                    $("#modal-body").html("<h2><b>OCCUPATION...</b></h2>");
                    $('td#occupation_display').text(data.occupation);
                }
                else if(response == 'work_updated'){
                    $("#loader_animation").hide();
                    $('h4.modal-title').text('NOTIFICATION');
                    $("#modal-body").html("<h2><b>WORK ADDRESS UPDATED...</b></h2>");
                    $('td#workaddress_display').text(data.work_address);
                }
                else{
                    $("#loader_animation").hide();
                    $('#form_body').html(response);   
                }
            	
                
            },
            error: function(){
                console.log("UH OH! SOMETHING WENT WRONG");
            }
        });
        return false;
    });
</script>