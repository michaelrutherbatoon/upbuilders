<script type="text/javascript">
	$( document ).ready(function() {
		$("#loader_animation").hide();
	    $('#error_notification').hide();

	    $('form.loginForm').on('submit', function(){
	        var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
	        that.find('[name]').each(function(index,value){
	            var that = $(this), name = that.attr('name'), value = that.val();
	            data[name] = value;
	        });
	        $.ajax({
	            url: url,
	            type: type,
	            data: data,
	            cache: false,
	            beforeSend: function(){ $("#loader_animation").show(); },
	            success: function(response){
	                $("#loader_animation").hide();
	                if(response == "false"){
	                    $("#loader_animation").hide();
	                    var failed_url = "<?php echo site_url('UB/access_failed'); ?>";
	                    $.ajax({
	                        url: failed_url,
	                        success: function(response){
	                            $('#loginPage').html(response);
	                            $('#error_notification').show();
	                            $("#loader_animation").hide();
	                        }
	                    });
	                }
	                else{
	                    window.location.href= response;
	                }   
	            },
	            error: function(){
	                console.log("UH OH! SOMETHING WENT WRONG");
	            }
	        });
	        return false;
	    });
	});
</script>