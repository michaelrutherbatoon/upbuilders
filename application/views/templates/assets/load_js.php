<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/wow/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>


<script type="text/javascript">
	$("#loader_animation").hide();
</script>
<script type="text/javascript">
	
	var socket = io.connect( 'http://'+window.location.hostname+':3000' );

    socket.on( 'new_count_message', function( data ) {
        $( "#new_count_message" ).html( data.new_count_message );
        $('#notif_audio')[0].play();
    });
    socket.on( 'new_message', function(data) {
    	console.log(data);
        <?php
            $current_date = date('Y-m-d H:i:s');
            $post_start_convert = date('h:i a m-d-Y', strtotime($current_date));
            $date_sent = explode(' ', $post_start_convert);
            $final = $date_sent[2]." at ".$date_sent[0]." ".strtoupper($date_sent[1]);
        ?>
        var final_time = "<?php echo $final; ?>";
        console.log(data.id);

        $( "#tbody_body" ).prepend(
            "<tr class='undread'>" +
                "<td class='check-mail'><input type='checkbox' class='i-checks'></td>" +
                "<td class='mail-ontact'>"+ data.name +"</td>" + 
                "<td class='mail-subject'>" + data.subject + "</td>" +
                "<td class='text-right mail-date'>"+ final_time +"</td>" +
            "</tr>");
  });
</script>
