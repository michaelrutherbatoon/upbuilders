<script type="text/javascript">
  $(document).ready(function(){
    $('div#success_message').hide();
    $("#loader_animation").hide();

    $("button#form_modal_close").click(function(event){
      console.log("CLOSED");
      // $("#modal_info_body").scrollTop(0);
      // $("#modal_form_body").scrollTop(0);
    });
    $("a#view_post_info").click(function(event){
        event.preventDefault(); 
        var myurl = $(this).attr("href");
        var id = $(this).attr("id");
        if(id == 'view_post_info'){
          $('h4#modal_title').text('Post Information');
        }
        else if(id == 'view_user_info'){
          $('h4#modal_title').text('Client Information');
        }
        else if(id == 'update_post'){
          $('h4#modal_title').text('Update Post');
        }
    $.ajax({
        url: myurl,
        success: function(msg) {
          $('#info_modal').modal('show');
          $('#info_modal_content').html(msg);

        }
      });
    });
    $("a#confirm_delete_post").click(function(event){
        event.preventDefault(); 
        var myurl = $(this).attr("href");
        var id = $(this).attr("id");
    $.ajax({
        url: myurl,
        success: function(msg) {
          $("#loader_animation").hide();
          $('#confirm_modal').modal('show');
          $('#confirm_body_modal').html(msg);

        }
      });
    });
    $("a#post_new_button, a#update_post, a#modal_update_post").click(function(event){
        $("#loader_animation").hide();
        event.preventDefault(); 
        var myurl = $(this).attr("href");
        var id = $(this).attr("id");
        if(id == 'post_new_button'){
          $('h1#label').text('New Post Form');
        }
    $.ajax({
        url: myurl,
        success: function(msg) {
          $("#loader_animation").hide();
          $('#form_modal').modal('show');
          $('#form_modal_content').html(msg);

        }
      });
    });
  });
</script>