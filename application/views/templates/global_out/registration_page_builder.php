<div id="form_client_reg" style="margin-top:70px;">
    <div>
      <div style="padding-left:50px; padding-right:50px;">
          <br><br>
          <h1 id="header_form"><strong>Builder Registration - UpBuilders</strong></h1>
          <div class="row">
          <div class="alert alert-info" id="error_notification" style="max-width:100vh;">
              <label>Note before registering as a builder.</label>
              <p>In order to register as a builder. You must have a record of your project in the OBO Office. You can verify it by filling up the "OBO Record Information" form below. Just give only one and you can add another OBO record in your account later.
              </p>
          </div>
            <?php 
            $attr = array('method' => 'POST', 'id' => 'builderRegForm');
            echo form_open('Registration/builder_registration_form_submit', $attr); ?>
              <div class="col-sm-4">
                  <h3>OBO Record Information</h3><br>
                  <div class="form-group">
                      <label>Employer's First Name</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('employer_name'); ?>
                      </span>
                      <input type="text" class="form-control" 
                      placeholder="Employer's First Name..." name="employer_fname"
                      value="<?php echo set_value('employer_name'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Employer's Last Name</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('employer_name'); ?>
                      </span>
                      <input type="text" class="form-control" name="employer_lname" 
                      id="employer_name" placeholder="Employer's Last Name..."
                      value="<?php echo set_value('employer_name'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Building Type</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('building_type'); ?>
                      </span>
                      <input type="text" class="form-control" name="building_type"
                      id="building_type">
                  </div>
                  <div class="form-group">
                      <label>Project Location</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('location'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Location Here..." name="location" id="location">
                  </div>
                  <div class="form-group">
                      <label>Project Engineer/Builder Name</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('engineer_name'); ?>
                      </span>
                      <input type="text" class="form-control" id="engineer_name" 
                      name="engineer_name" placeholder="Type Engineer Name Here..." 
                      value="<?php echo set_value('engineer_name'); ?>">
                  </div>
                  <input type="submit" class="btn btn-primary btn-sm" vaue="Submit"
                  style="font-weight: bold;">
              </div>
              <!-- <div class="col-sm-4">
                  <h3>Personal Information</h3><br>
                  <div class="form-group">
                      <label>First Name</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('first_name'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type First Name Here..."
                      id="first_name" name="first_name" value="<?php echo set_value('first_name'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Last Name</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('last_name'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Last Name Here..."
                      id="last_name" name="last_name" value="<?php echo set_value('last_name'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Gender</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('gender'); ?>
                      </span>
                      <select class="form-control" name="gender" id="gender">
                          <option value="">Gender</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                      </select>
                  </div>
                  <div class="form-group">
                        <label>Birth Date</label>
                        <span style="color:red; font-weight: bold;">
                        <?php echo form_error('birth_date'); ?>
                      </span>
                        <input type="date" class="form-control" id="birth_date" name="birth_date"
                        value="<?php echo set_value('birth_date'); ?>">
                  </div>
              </div>
              <div class="col-sm-4">
                  <h3>Other Information</h3><br>
                  <div class="form-group">
                      <label>Occupation</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('occupation'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Occupation Here..."
                      name="occupation" id="occupation" value="<?php echo set_value('occupation'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Work Address</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('work_address'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Work Address Here..."
                      name="work_address" id="work_address" value="<?php echo set_value('work_address'); ?>">
                  </div><br>
                  <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
                      <div class="sk-rect1"></div>
                      <div class="sk-rect2"></div>
                      <div class="sk-rect3"></div>
                      <div class="sk-rect4"></div>
                      <div class="sk-rect5"></div>
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block" style="margin-top:5px; font-weight:bold;">
                          Submit Form
                      </button>
                  </div>                
              </div> -->
              <?php form_close(); ?>
          </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  $("#loader_animation").hide();

  $('form#builderRegForm').on('submit', function(){
      var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
      that.find('[name]').each(function(index,value){
          var that = $(this), name = that.attr('name'), value = that.val();
          data[name] = value;
      });
      $.ajax({
          url: url,
          type: type,
          data: data,
          cache: false,
          beforeSend: function(){ $("#loader_animation").show(); },
          success: function(response){
              $("#loader_animation").hide();
              // $('#form_client_reg').html(response); 
              console.log(response);         
          },
          error: function(){
              console.log("UH OH! SOMETHING WENT WRONG");
          }
      });
      return false;
  });
</script>