<head>
    <title>UpBuilders</title>
    <?php $this->load->view($load_css); ?>
    <style type="text/css">
    #nav_text{
        font-weight: bold;
        color:white;
    }
    #nav_text:hover{
        color:#1AB394;
    }
</style>
</head>
<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar_design">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" id="navbar_brand" href="index.html"><strong>UpBuilders</strong></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="color:#233645; margin-top:5px;">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a id="nav_text" href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a id="nav_text"
                        href="#features">About</a></li>
                        <li><a id="nav_text" 
                        href="#team">Contact</a></li>
                        <li><a id="nav_text"
                        href="<?php echo site_url('UB/login'); ?>">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
<body class="gray-bg landing-page" style="height:80vh;">
    <div class="animated fadeInDown">
        <?php $this->load->view($content); ?>
    </div>
</body>
<?php $this->load->view($load_js); ?>
