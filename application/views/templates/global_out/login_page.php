<div id="loginPage">
    <div class="loginColumns">  
        <div class="row" style="margin-top:50px;">

            <div class="col-md-6">
                <h2 class="font-bold">Welcome to UpBuilders</h2>
            </div>

            <div ng-controller="global" class="col-md-6">
                <div class="ibox-content" style="border-radius: 10px; border: 1px solid #EEEEEE;">
                    <div class="alert alert-danger" id="error_notification">
                        <strong>Invalid username or password...</strong>
                    </div>
                    <label style="color:red;" id="invalidAccount_message"></label>
                    <?php 
                    $attr = array('method' => 'POST', 'class' => 'loginForm');
                    echo form_open('UB/verifyLogin', $attr); ?>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Username" 
                            id="username" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" 
                            id="password" required>
                        </div>
                        <div id="loader_animation" class="sk-spinner sk-spinner-wave" 
                        style="margin-bottom:10px;">
                              <div class="sk-rect1"></div>
                              <div class="sk-rect2"></div>
                              <div class="sk-rect3"></div>
                              <div class="sk-rect4"></div>
                              <div class="sk-rect5"></div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-block" value="Login Account">
                    <?php echo form_close(); ?>

                    <a href="#">
                        <small>Forgot password?</small>
                    </a>
                    <p class="text-muted text-center">
                        <small>Do not have an account?</small>
                    </p>
                    <a class="btn btn-sm btn-white btn-block" data-toggle="modal" data-target="#myModal">
                        Create an account
                    </a>
                </div>
            </div>
        </div>
        <hr/>
    </div>  
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:340px;">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">WHAT ARE YOU?</h4>
              </div>
                <div class="modal-body" style="height:175px;">
                    <div style="float:left;">
                        <a class="btn btn-sm btn-white btn-block" 
                        href="<?php echo site_url('UB/builder_registration_form'); ?>">
                        <img src="<?php echo base_url(); ?>assets/images/system_images/builder.png"
                        style="width:100px; height:100px;"><br>
                        <center><label style="margin-top:5px;">Builder</label></center>
                        </a>
                    </div>

                    <div style="float:right;">
                        <a class="btn btn-sm btn-white btn-block" 
                        href="<?php echo site_url('UB/client_registration_form'); ?>">
                        <img src="<?php echo base_url(); ?>assets/images/system_images/businessman.png"
                        style="width:100px; height:100px;"><br>
                        <center><label style="margin-top:5px;">Client</label></center>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>

        </div>
    </div>
</div>
