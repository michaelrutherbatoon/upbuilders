<div id="form_client_reg" style="margin-top:70px;">
    <div>
      <div style="padding-left:50px; padding-right:50px;">
          <br><br>
          <h1 id="header_form"><strong>Client Registration - UpBuilders</strong></h1><br><br>
          <div class="row">
            <?php 
            $attr = array('method' => 'POST', 'id' => 'clientRegForm');
            echo form_open('Registration/client_registration_form_submit', $attr); ?>
              <div class="col-sm-4">
                  <h3>Account Information</h3><br>
                  <div class="form-group">
                      <label>Username</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('username'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Username Here..."
                      name="username" id="username" value="<?php echo set_value('username'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Password</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('password'); ?>
                      </span>
                      <input type="password" class="form-control" placeholder="Type Password Here..." name="password"
                      id="password">
                  </div>
                  <div class="form-group">
                      <label>Confirm Password</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('pass_conf'); ?>
                      </span>
                      <input type="password" class="form-control" placeholder="Confirm Password Here..." name="pass_conf"
                      id="pass_conf">
                  </div>
                  <div class="form-group">
                      <label>Email Address</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('email'); ?>
                      </span>
                      <input type="email" class="form-control" placeholder="Type Email Address Here..."
                      id="email" name="email" value="<?php echo set_value('email'); ?>">
                  </div>

                  <label>Contact Number: </label>
                  <span style="color:red; font-weight: bold;">
                        <?php echo form_error('contact'); ?>
                      </span>
                  <div class="input-group">
                      <span class="input-group-addon">Philippines (+63)</span>
                      <input type="number" class="form-control" id="contact" name="contact" 
                      placeholder="Example: 9261718842" value="<?php echo set_value('contact'); ?>"><br>
                  </div>
              </div>
              <div class="col-sm-4">
                  <h3>Personal Information</h3><br>
                  <div class="form-group">
                      <label>First Name</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('first_name'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type First Name Here..."
                      id="first_name" name="first_name" value="<?php echo set_value('first_name'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Last Name</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('last_name'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Last Name Here..."
                      id="last_name" name="last_name" value="<?php echo set_value('last_name'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Gender</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('gender'); ?>
                      </span>
                      <select class="form-control" name="gender" id="gender">
                          <option value="">Gender</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                      </select>
                  </div>
                  <div class="form-group">
                        <label>Birth Date</label>
                        <span style="color:red; font-weight: bold;">
                        <?php echo form_error('birth_date'); ?>
                      </span>
                        <input type="date" class="form-control" id="birth_date" name="birth_date"
                        value="<?php echo set_value('birth_date'); ?>">
                  </div>
              </div>
              <div class="col-sm-4">
                  <h3>Other Information</h3><br>
                  <div class="form-group">
                      <label>Occupation</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('occupation'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Occupation Here..."
                      name="occupation" id="occupation" value="<?php echo set_value('occupation'); ?>">
                  </div>
                  <div class="form-group">
                      <label>Work Address</label>
                      <span style="color:red; font-weight: bold;">
                        <?php echo form_error('work_address'); ?>
                      </span>
                      <input type="text" class="form-control" placeholder="Type Work Address Here..."
                      name="work_address" id="work_address" value="<?php echo set_value('work_address'); ?>">
                  </div><br>
                  <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
                      <div class="sk-rect1"></div>
                      <div class="sk-rect2"></div>
                      <div class="sk-rect3"></div>
                      <div class="sk-rect4"></div>
                      <div class="sk-rect5"></div>
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block" style="margin-top:5px; font-weight:bold;">
                          Submit Form
                      </button>
                  </div>                
              </div>
              <?php form_close(); ?>
          </div>
      </div>
    </div>
</div>
