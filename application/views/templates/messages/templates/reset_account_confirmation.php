<div id="confirmation_form">
  <center>
  	<h3>Are You Sure?</h3>
  	<?php 
  	$att = array(
  		'method' => 'POST',
  		'id' => 'confirm');
  	echo form_open('Update/confirm_reset_response', $att);
  	 ?>
  	<input type="hidden" name="account_id" value="<?php echo $id; ?>">
  	<div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
          <div class="sk-rect1"></div>
          <div class="sk-rect2"></div>
          <div class="sk-rect3"></div>
          <div class="sk-rect4"></div>
          <div class="sk-rect5"></div>
      </div>
  	<button type="submit" class="btn btn-primary" style="width:10vh;">Yes</button>
  	<button class="close-modal no_button btn btn-danger" style="width:10vh;" data-dismiss="modal">
      No
    </button>
  	<?php echo form_close(); ?>
  	
  </center>
</div>
<script type="text/javascript">
	$("#loader_animation").hide();

	$('form#confirm').on('submit', function(){
      var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
      that.find('[name]').each(function(index,value){
          var that = $(this), name = that.attr('name'), value = that.val();
          data[name] = value;
      	});
      	$.ajax({
          	url: url,
          	type: type,
          	data: data,
          	cache: false,
          	beforeSend: function(){ $("#loader_animation").show(); },
          	success: function(response){
                if(response == 'success'){
                  $("#loader_animation").hide(); 
                  $('.modal').modal('hide');
                }
              	            
          	},
          	error: function(){
              	console.log("UH OH! SOMETHING WENT WRONG");
          	}
      	});
      	return false;
  	});
</script>