<div id="confirmation_form">
  <center>
  	<h3>Are You Sure?</h3>
  	<?php 
  	$att = array(
  		'method' => 'POST',
  		'id' => 'confirm');
  	echo form_open('Update/delete_post_response', $att);
  	 ?>
    <input type="hidden" name="post_id" value="<?php echo $id; ?>">
  	<div id="loader_animation_2" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
          <div class="sk-rect1"></div>
          <div class="sk-rect2"></div>
          <div class="sk-rect3"></div>
          <div class="sk-rect4"></div>
          <div class="sk-rect5"></div>
      </div>
  	<button type="submit" class="btn btn-primary" style="width:10vh;">Yes</button>
  	<button class="close-modal no_button btn btn-danger" style="width:10vh;" data-dismiss="modal">No
    </button>
  	<?php echo form_close(); ?>
  	
  </center>

<script type="text/javascript">
	$("#loader_animation_2").hide();
  $('button#no_button').click(function(){
    console.log("YAWA KA");
  });
  $("button.no_button").click(function(event){
      $("#loader_animation_2").hide();
  });
	$('form#confirm').on('submit', function(){
      var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
      that.find('[name]').each(function(index,value){
          var that = $(this), name = that.attr('name'), value = that.val();
          data[name] = value;
      	});
        var post_id = data.post_id;
      	$.ajax({
          	url: url,
          	type: type,
          	data: data,
          	cache: false,
          	beforeSend: function(){ $("#loader_animation_2").show(); },
          	success: function(response){
            	$("#loader_animation_2").hide();
              	if(response == 'delete_success'){
                  $("div#contain"+post_id).remove();
                  $('#newsfeed-modal').modal('hide');
                  $('#confirm-modal').modal('hide');
                  $('body').removeClass('modal-open');
                  $('.modal-backdrop').remove();
                  $("#loader_animation_2").hide();
              	}
              	else{
                	$('#confirmation_form').html(response); 
                	$("#loader_animation_2").hide();
              	}
              	             
          	},
          	error: function(){
              	console.log("UH OH! SOMETHING WENT WRONG");
          	}
      	});
      	return false;
  	});
</script>
</div>