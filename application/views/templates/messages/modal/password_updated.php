<script type="text/javascript">
    $(window).load(function(){
        $('#passwod_updatedModal').modal('show');
    });
</script>
<div id="passwod_updatedModal" class="modal inmodal" role="dialog" aria-hidden="true" tabindex="-1">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content animated fadeIn">
      <div class="modal-body" style="color:green;">
        <center>
        	<span class="fa fa-check-circle" style="font-size: 10em;"></span>
        	<h3>YOUR PASSWORD HAS BEEN UPDATED PLEASE LOGIN YOUR ACCOUNT AGAIN WITH YOUR NEW PASSWORD</h3>
        </center>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
