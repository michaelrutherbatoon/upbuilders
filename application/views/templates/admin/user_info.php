<div id="form_body">
<?php 
	foreach($profile_data as $profile_data){
		$full_name = $profile_data['first_name']." ".$profile_data['last_name'];
		$gender = $profile_data['gender'];
		$birth_date = $profile_data['birth_date'];
	}
	foreach($account_data as $account_data){
		$username = $account_data['username'];
		$email = $account_data['email'];
		$contact = $account_data['contact'];
		$privilege = $account_data['privilege'];
		$status = $account_data['is_active'];
		$profile_pic = $account_data['profile_pic'];
	} ?>
	<img src="<?php echo base_url(); ?>assets/images/profile_images/profiles/<?php echo $profile_pic; ?>"
	style="width:150px; height:150px;"><br><br>
	<table class="table table-striped table-bordered" style="max-width:100vh;">
    	<tbody>
		    <tr>
		       	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Full Name</th>
		       	<td style="border:0px; border-bottom: 1px solid #E5E5E5;"><?php echo $full_name; ?></td>
		  	</tr>
		    <tr>
		      	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Gender</th>
		      	<td style="border:0px; border-bottom: 1px solid #E5E5E5;"><?php echo $gender; ?></td>
		    </tr>
		    <tr>
		       	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Birth Date</th>
		       	<td style="border:0px; border-bottom: 1px solid #E5E5E5;"><?php echo $birth_date; ?></td>
		    </tr>
	       	<tr>
		      	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Username</th>
		       	<td style="border:0px; border-bottom: 1px solid #E5E5E5;"><?php  echo $username; ?></td>
		    </tr>
		    <tr>
		      	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Email Address</th>
		       	<td style="border:0px; border-bottom: 1px solid #E5E5E5;"><?php  echo $email; ?></td>
		    </tr>
		    <tr>
		      	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Contact Number</th>
		      	<td style="border:0px; border-bottom: 1px solid #E5E5E5;">+63<?php  echo $contact; ?></td>
		    </tr>
		    <?php if($this->session->privilege == 'Admin'){ ?>
		    	<tr>
			       	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Account Privilege</th>
			       	<td style="border:0px; border-bottom: 1px solid #E5E5E5;"><?php  echo $privilege." Account"; ?></td>
			    </tr>
		    	<tr>
			       	<th style="border:0px; border-bottom: 1px solid #E5E5E5;">Account Status</th>
			       	<?php if($status == 1){ ?>
			       		<td style="border:0px; border-bottom: 1px solid #E5E5E5;">Enabled</td>
			       	<?php }
			       	else{ ?>
			       		<td style="border:0px; border-bottom: 1px solid #E5E5E5;">Disabled</td>
			       	<?php } ?>	
			    </tr>
		    <?php } ?>
      	</tbody>
    </table>
</div>