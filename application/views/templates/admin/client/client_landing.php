<div id="account-table">   
  <table class="table table-bordered table-hover " id="editable">
      <thead>
          <tr>
            <th>Full Name</th>
            <th>Account Status</th>
            <th style="width:100px; border-left: 3px solid #F5F5F6;"></th>
          </tr>
      </thead>
      <tbody class="tooltip-demo">
        <?php foreach($table_data as $data){
              $id = $data['account_id'];
              $status = $data['is_active']; 
              if($status == 1){
                $stat = 'Enabled';
              }
              else{
                $stat = 'Disabled';
              }
              ?>
          <tr style="background-color:#F9F9F9;">
              <td><a href='<?php echo site_url("Modal_Bodies/account_info/$id"); ?>' id="view_info"
              style="font-weight: bold; text-decoration: underline;"><?php echo $data['first_name']." ".$data['last_name']; ?></a></td>
              <!-- <td>
                  <a href='<?php echo site_url("Account/account_info/$id"); ?>' id="view_info" data-toggle="tooltip" 
                  data-placement="bottom" title="View Full Info." >
                    <?php echo $data['first_name']." ".$data['last_name']; ?>
                  </a>  
              </td> -->
              <td><?php echo $stat; ?></td>
              <td style="text-align: center;">
                <?php 
                ?>
              <div class="tooltip-demo">
                  <?php if($status == 1){ ?>
                    <a href="<?php echo site_url("Modal_Bodies/confirm_change_status_message/$id"); ?>" 
                    class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" 
                    title="Disable" id="show_confirmation_status">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                  <?php }
                    elseif($status == 0){ ?>
                    <a href="<?php echo site_url("Modal_Bodies/confirm_change_status_message/$id"); ?>" 
                    class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" 
                    title="Enable" id="show_confirmation_status">
                        <span class="glyphicon glyphicon-ok"></span>
                    </a>
                  <?php } ?>
                  <a href="<?php echo site_url("Modal_Bodies/confirm_reset_message/$id"); ?>" class="btn btn-warning" 
                  data-toggle="tooltip" data-placement="bottom" title="Reset" id="show_confirmation_reset">
                      <span class="fa fa-rotate-right"></span>
                  </a>
                </div>
              </td>  
          </tr>
          <?php } ?>
      </tbody>
  </table>
</div>
<div class="modal inmodal" id="admin_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <h4 class="modal-title" id="modal_title">Modal title</h4>
            </div>
            <div class="modal-body" id="body_modal" style="max-height:60vh; overflow-y: auto;">
            </div>
            <div class="modal-footer">
                <a class="close-modal btn btn-danger" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!-- CONFIRM MODAL -->
<div class="modal" id="confirmation_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm" style="margin-top: 150px;">
        <div class="modal-content">
            <div class="modal-body" id="confirm_body_modal">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
      $("a#add_admin_modal, a#view_info").click(function(event){
          event.preventDefault(); 
          var myurl = $(this).attr("href");
          var id = $(this).attr("id");
          if(id == 'add_admin_modal'){
            $('h4#modal_title').text('Admin Registration Form');
          }
          if(id == 'view_info'){
            $('h4#modal_title').text('User Information');
          }
      $.ajax({
          url: myurl,
          success: function(msg) {
            $('#admin_modal').modal('show');
            $('#body_modal').html(msg);
          }
        });
      
      });


      $("a#show_confirmation_status, a#show_confirmation_reset").click(function(event){
          event.preventDefault(); 
          var myurl = $(this).attr("href");
          var id = $(this).attr("id");
          if(id == 'add_obo_modal'){
            $('h4#modal_title').text('OBO Registration Form');
          }
          if(id == 'view_info'){
            $('h4#modal_title').text('User Information');
          }
      $.ajax({
          url: myurl,
          success: function(msg) {
            $('#confirmation_modal').modal('show');
            $('#confirm_body_modal').html(msg);
          }
        });
      });
  });
</script>