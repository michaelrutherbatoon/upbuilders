<div id="form_obo_reg">
	<?php 
	$attr = array(
		'method' => 'POST',
		'id' => 'obo_form');
	echo form_open('Registration/admin_registration_form_submit', $attr); ?>
	<label>First Name</label>
	<span style="color:red; font-weight:bold;"><?php echo form_error('first_name'); ?></span>
	<input type="text" class="form-control" name="first_name" placeholder="Type First Name Here..." 
	value="<?php echo set_value('first_name'); ?>"><br>
	<label>Last Name</label>
	<span style="color:red; font-weight:bold;"><?php echo form_error('last_name'); ?></span>
	<input type="text" class="form-control" name="last_name" placeholder="Type Last Name Here..."
	value="<?php echo set_value('last_name'); ?>"><br>
	<label>Gender</label>
	<span style="color:red; font-weight:bold;"><?php echo form_error('gender'); ?></span>
	<select class="form-control" name="gender">
		<option value="">Gender</option>
		<option value="Male">Male</option>
		<option value="Female">Female</option>
	</select><br>
	<label>Birth Date</label>
	<span style="color:red; font-weight:bold;"><?php echo form_error('birth_date'); ?></span>
	<input type="date" class="form-control" name="birth_date" value="<?php echo set_value('birth_date'); ?>"><br>
	<label>Email</label>
	<span style="color:red; font-weight:bold;"><?php echo form_error('email'); ?></span>
	<input type="email" class="form-control" name="email" placeholder="Type Email Address Here"
	value="<?php echo set_value('email'); ?>"><br>
	<label>Contact Number</label>
	<span style="color:red; font-weight:bold;"><?php echo form_error('contact'); ?></span>
	<div class="input-group">
        <span class="input-group-addon">Philippines (+63)</span>
        <input type="number" class="form-control" id="contact" name="contact" 
        placeholder="Example: 9261718842" value="<?php echo set_value('contact'); ?>"><br>
    </div>

	

	<br>
	<!-- ADNIMATION FOR LOADING -->
    <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
	<input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" value="Submit Registration Form"
	style="font-weight:bold;">

	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
  $("#loader_animation").hide();

  $('form#obo_form').on('submit', function(){
      var that = $(this), url = that.attr('action'), type = that.attr('method'), data = {};
      that.find('[name]').each(function(index,value){
          var that = $(this), name = that.attr('name'), value = that.val();
          data[name] = value;
      });
      $.ajax({
          url: url,
          type: type,
          data: data,
          cache: false,
          beforeSend: function(){ $("#loader_animation").show(); },
          success: function(response){
          	if(response == 'success'){
          		$('.modal').modal('hide');
          		$('#account-table').load('refresh_admin_table');
          	}
          	else{
          		$("#loader_animation").hide();
              	$('#form_obo_reg').html(response); 
          	}
                           
          },
          error: function(){
              console.log("UH OH! SOMETHING WENT WRONG");
          }
      });
      return false;
  });
</script>