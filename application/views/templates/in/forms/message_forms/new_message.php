<style type="text/css">
	.btn{
		font-weight: bold;
	}
</style>
<?php  
$att = array(
	'id' => 'message-form',
	'method' => 'POST');
echo form_open('Registration/submit_message', $att);
?>
<label>To: </label>
<input type="email" class="form-control" name="to" id="to"><br>
<label>Title: </label>
<input type="title" class="form-control" name="title" id="title"><br>
<label>Content</label>
<textarea class="form-control" name="content" id="content" style="height:100px;">
</textarea><br>
<input type="submit" class="btn btn-block btn-primary" name="submit">
<?php  
echo form_close();
?>
<script type="text/javascript">
	$('form#message-form').on('submit', function(){
        var url_dest = $(this).attr('action');

        var to = document.getElementById('to').value;
        var title = document.getElementById('title').value;
        var content = document.getElementById('content').value;

        var formData = new FormData();
        formData.append("title", title);
        formData.append("to", to);
        formData.append("content", content);


        $.ajax({
            url: url_dest,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            // beforeSend: function(){ $("#loader_animation").show(); },
            success: function(data){
            	data = JSON.parse(data);
                var socket = io.connect( 'http://'+window.location.hostname+':3000' );
                socket.emit('new_count_message', { 
                  new_count_message: data.new_count_message
                });
                socket.emit('new_message', { 
                  name: "<?php echo $this->session->full_name; ?>",
                  subject: title,
                  id: data.id
                });

	             
                
            },
            error: function(){
                console.log("UH OH! SOMETHING WENT WRONG");
            }
        });
        return false;
    });
</script>