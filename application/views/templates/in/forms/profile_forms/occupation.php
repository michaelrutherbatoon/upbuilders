<div id="form_body">
	<?php 
	$attr = array(
		'method' => 'POST',
		'id' => 'update_profile_form');
	echo form_open('Update/occupation_submit', $attr); ?>
	<label>Occupation</label>
	<label id="error_message"><?php echo form_error('occupation'); ?></label>
	<input type="text" class="form-control" name="occupation" placeholder="Type Occupation Here...">
	<br>
    <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
	<input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" value="Update Occupation"
	style="font-weight:bold;">

	<?php echo form_close(); ?>
</div>