<div id="form_body">
	<?php 
	$attr = array(
		'method' => 'POST',
		'id' => 'update_profile_form');
	echo form_open('Update/fullname_submit', $attr); ?>
	<label>First Name</label>
	<label id="error_message"><?php echo form_error('first_name'); ?></label>
	<input type="text" class="form-control" name="first_name" placeholder="Type First Name Here...">
	<br>
	<label>Last Name</label>
	<label id="error_message"><?php echo form_error('last_name'); ?></label>
	<input type="text" class="form-control" name="last_name" placeholder="Type Last Name Here">
	<br>
    <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
	<input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" value="Update Full Name"
	style="font-weight:bold;">

	<?php echo form_close(); ?>
</div>