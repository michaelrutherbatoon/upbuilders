<div id="form_body">
	<?php 
	$attr = array('method' => 'POST', 'id' => 'update_profile_form');
	echo form_open('Update/workaddress_submit', $attr); ?>
	    <label>Work Address</label>
	    <label id="error_message"><?php echo form_error('work_address'); ?></label>
	    <input type="text" class="form-control" name="work_address" 
        placeholder="Type Work Address Here...">
	    <br>
        <div id="loader_animation" class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
	    <input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" 
        value="Update Work Address">

	<?php echo form_close(); ?>
</div>