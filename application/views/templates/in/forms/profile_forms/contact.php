<div id="form_body">
	<?php 
	$attr = array(
		'method' => 'POST',
		'id' => 'update_profile_form');
	echo form_open('Update/contact_submit', $attr); ?>
	<label>Contact Number</label>
	<label id="error_message"><?php echo form_error('contact'); ?></label>
	<div class="input-group">
        <span class="input-group-addon">Philippines (+63)</span>
        <input type="number" class="form-control" name="contact" placeholder="Example: 9261718842"><br>
    </div>
	<br>
    <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
	<input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" value="Update Contact Number"
	style="font-weight:bold;">

	<?php echo form_close(); ?>
</div>