<div id="form_body">
    <style type="text/css"></style>
	<?php 
	$attr = array(
		'method' => 'POST',
		'id' => 'update_profile_form');
	echo form_open('Update/password_submit', $attr); ?>
	<label>Old Password</label>
	<label id="error_message"><?php echo form_error('old_password'); ?></label>
	<input type="password" class="form-control" name="old_password" placeholder="Type Old Password Here...">
	<br>
	<label>New Password</label>
	<label id="error_message"><?php echo form_error('new_password'); ?></label>
	<input type="password" class="form-control" name="new_password" placeholder="Type New Password Here...">
	<br>
	<label>Confirm New Password</label>
	<label id="error_message"><?php echo form_error('password_confirmation'); ?></label>
	<input type="password" class="form-control" name="password_confirmation" 
	placeholder="Confirm The New Password Here...">
	<br>
	<div id="loader_animation" class="sk-spinner sk-spinner-wave">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
	<input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" value="Update Password"
	style="font-weight:bold;">

	<?php echo form_close(); ?>
</div>