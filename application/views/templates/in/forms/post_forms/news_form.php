<div id="form_body">
<?php 
?>
<style type="text/css"> 
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    #error{
        color:red;
        margin-left: 5px;
        margin-bottom:-5px;
    }
</style>
<?php 
    $attr = array(
        'method' => 'POST',
        'id' => 'post_form',
        'enctype' => 'multipart/form-data');
    echo form_open('Registration/add_new_post_form_submit', $attr); ?>
        <label>Post Title</label><label id="error"><?php echo form_error('title'); ?></label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Type Title Here..."><br>
        <label>Project Type</label><label id="error"><?php echo form_error('project_type'); ?>
        </label>
        <select class="form-control" name="project_type" id="project_type">
            <option value="">Select Project Type</option>
            <option value="Residential">Residential Project</option>
            <option value="Commercial">Commercial Project</option>
        </select><br>
        <label>Description</label><label id="error"><?php echo form_error('desc'); ?></label>
        <textarea class="form-control" name="desc" id="desc" style="height: 100px;"
        placeholder="Type Post Description Here..."></textarea><br>
        <label>Land Description</label><label id="error"><?php echo form_error('land_desc'); ?>
        </label>
        <textarea class="form-control" name="land_desc" id="land_desc" style="height: 100px;"
        placeholder="Type Land Description Here..."></textarea><br>
        <br>
        <label>Expiration Date (Leave it blank if you dont want to set and expiration date)</label>
        <label id="error"><?php echo form_error('exp_date'); ?></label>
        <input type="date" class="form-control" name="exp_date" id="exp_date"><br>
        <?php if(isset($file_error)){ ?>
        <center>
            <label id="error" style="margin-bottom:5px;">(Please select an image of your land.)</label>
        </center>
        <?php } ?>
        <span class="btn btn-info btn-file btn-block" style="font-weight: bold;">
            Select Land Images <input type="file" id="file" name="file[]" multiple="multiple">
        </span>
        <div id="preview" style="text-align: center; margin-top:20px;"></div><br>
        <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" 
        style="font-weight:bold;" value="SUBMIT POST">
<?php 
    echo form_close();
 ?>
</div>
<script type="text/javascript">
    $("#loader_animation").hide();

    function previewImages() {
        var files = document.getElementById('file').files;
        if(files.length > 3)
        {
            alert("Upload images must not be greater than 3.");
        }
        else{
            var preview = document.querySelector('#preview'); 
            if (this.files) {
                [].forEach.call(this.files, readAndPreview);
            }

            function readAndPreview(file) {
            // Make sure `file.name` matches our extensions criteria

                if (!/\.(jpe?g|png)$/i.test(file.name)) {
                    return alert(file.name + " is not an image");
                } // else...
                else if(file.length > 3){
                    console.log("TULO RA OI GARA SAD");
                }
                else{
                   $("#preview").html(null);
                    var reader = new FileReader();
                
                    reader.addEventListener("load", function() {
                        var image = new Image();
                        image.height = 120;
                        image.width = 170;
                        image.title  = file.name;
                        image.src    = this.result;
                        preview.appendChild(image);
                    }, 
                    false);
                    reader.readAsDataURL(file); 
                }
                
            }
        }
    }
    document.querySelector('#file').addEventListener("change", previewImages, false);

    $('form#post_form').on('submit', function(){
        var url_dest = $('#post_form').attr('action');

        var title = document.getElementById('title').value;
        var desc = document.getElementById('desc').value;
        var land_desc = document.getElementById('land_desc').value;
        var exp_date = document.getElementById('exp_date').value;
        var project_type = document.getElementById('project_type').value;

        var inputFile = $('#file');
        var filesToUpload = inputFile[0].files;
        if(filesToUpload.length > 3){
            console.log("MAXIMUM OF 3 IMAGES ONLY");
        }
        else{
            var formData = new FormData();
            for(var i = 0; i < filesToUpload.length; i++){
                var file = filesToUpload[i];
                formData.append("file[]",file, file.name);
            }
            formData.append("title", title);
            formData.append("desc", desc);
            formData.append("land_desc", land_desc);
            formData.append("exp_date", exp_date);
            formData.append("project_type", project_type);
            $.ajax({
                url: url_dest,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function(){ $("#loader_animation").show(); },
                success: function(response){
                    if(response == 'true'){
                        window.location.href = "<?php echo site_url('Account/timeline_page'); ?>";
                    }
                    else{
                        $("#loader_animation").hide();
                        $('#form_body').html(response);
                    }
                    
                    
                },
                error: function(){
                    console.log("UH OH! SOMETHING WENT WRONG");
                }
            });
            return false;
        }
    });
   
</script>