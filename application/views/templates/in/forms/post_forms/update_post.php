<div id="update_post_form_body">
<style type="text/css">
    
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#error{
    color:red;
    margin-left: 5px;
    margin-bottom:-5px;
}
</style>
<?php 
    $attr = array(
        'method' => 'POST',
        'id' => 'post_form',
        'enctype' => 'multipart/form-data');
    echo form_open('Update/post_update_form_submit', $attr); ?>
        <input type="hidden" name="post_id" id="post_id" value="<?php echo $data['id']; ?>">
        <input type="hidden" name="account_id" id="account_id" 
        value="<?php echo $data['account_id']; ?>">

        <label>Post Title</label><label id="error"><?php echo form_error('title'); ?></label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Type Title Here..." value="<?php echo $data['title']; ?>"><br>
        <label>Project Type</label>
        <label id="error"><?php echo form_error('project_type'); ?></label>
        <select class="form-control" name="project_type" id="project_type">
            <?php if($data['type'] == 'Commercial'){ ?>
                <option value="Commercial">Commercial Project</option>
                <option value="Residential">Residential Project</option>
            <?php }elseif($data['type'] == 'Residential'){ ?>
                <option value="Residential">Residential Project</option>
                <option value="Commercial">Commercial Project</option>
            <?php }else{ ?>
            <option value="">Select Project Type</option>
            <option value="Residential">Residential Project</option>
            <option value="Commercial">Commercial Project</option>
            <?php } ?>
        </select><br>
        <label>Description</label>
        <label id="error"><?php echo form_error('desc'); ?></label>
        <textarea class="form-control" name="desc" id="desc" style="height: 100px;"
        placeholder="Type Post Description Here..." required><?php echo $data['description']; ?></textarea><br>
        <label>Land Description</label>
        <label id="error"><?php echo form_error('land_desc'); ?></label>
        <textarea class="form-control" name="land_desc" id="land_desc" style="height: 100px;"
        placeholder="Type Land Description Here..." required><?php echo $data['land_description']; ?></textarea><br>
        <br>
        <?php
            
        ?>
        <label>Expiration Date (Leave it blank if you dont want to set an expiration date)</label>
        <label id="error"><?php echo form_error('exp_date'); ?></label>
        <input type="date" class="form-control" name="exp_date" value="<?php echo 
        $data['post_end']; ?>" id="exp_date"><br><br>
        <center>
            <label>Note: Leave the image upload if you dont want to update your images...</label>
        </center>
        <span class="btn btn-info btn-file btn-block" style="font-weight: bold;">
            Select Land Images <input type="file" id="file" name="file[]" multiple="multiple">
        </span>
        <div id="preview" style="text-align: center; margin-top:20px;">
 <!--             <?php 
                $images = $this->Model_Privileges->fetch_post_file($data['id']);
                foreach($images as $images){
            ?>
                <img src="<?php echo base_url(); ?>assets/images/post_images/<?php echo $images['file_name']; ?>"
                style="width:170px; height:120px;">
            <?php
                }
              ?> -->
        </div><br>
        <div id="loader_animation" class="sk-spinner sk-spinner-wave" style="margin-bottom:10px;">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <input type="submit" class="btn btn-primary btn-block" name="submit" id="submit" 
        style="font-weight:bold;" value="SUBMIT POST">
<?php 
    echo form_close();
 ?>

<script type="text/javascript">
    $("#loader_animation").hide();
    $("#back_loader_animation").hide();
    $("button#newsfeed-cancel").show();

    function previewImages() {
        var files = document.getElementById('file').files;
        if(files.length > 3)
        {
            alert("Upload images must not be greater than 3.");
        }
        else{
            var preview = document.querySelector('#preview'); 
            if (this.files) {
                [].forEach.call(this.files, readAndPreview);
            }

            function readAndPreview(file) {
            // Make sure `file.name` matches our extensions criteria

                if (!/\.(jpe?g|png)$/i.test(file.name)) {
                    return alert(file.name + " is not an image");
                } // else...
                else if(file.length > 3){
                    console.log("TULO RA OI GARA SAD");
                }
                else{
                   $("#preview").html(null);
                    var reader = new FileReader();
                
                    reader.addEventListener("load", function() {
                        var image = new Image();
                        image.height = 120;
                        image.width = 170;
                        image.title  = file.name;
                        image.src    = this.result;
                        preview.appendChild(image);
                    }, 
                    false);
                    reader.readAsDataURL(file); 
                }
                
            }
        }
    }
    document.querySelector('#file').addEventListener("change", previewImages, false);

    $('form#post_form').on('submit', function(){
        var url_dest = $('#post_form').attr('action');

        var title = document.getElementById('title').value;
        var desc = document.getElementById('desc').value;
        var land_desc = document.getElementById('land_desc').value;
        var exp_date = document.getElementById('exp_date').value;
        var project_type = document.getElementById('project_type').value;
        var post_id = document.getElementById('post_id').value;
        var account_id = document.getElementById('account_id').value;

        var inputFile = $('#file');
        var filesToUpload = inputFile[0].files;
        if(filesToUpload.length > 3){
            console.log("MAXIMUM OF 3 IMAGES ONLY");
        }
        else{
            var formData = new FormData();
            for(var i = 0; i < filesToUpload.length; i++){
                var file = filesToUpload[i];
                formData.append("file[]",file, file.name);
            }
            formData.append("title", title);
            formData.append("desc", desc);
            formData.append("land_desc", land_desc);
            formData.append("exp_date", exp_date);
            formData.append("project_type", project_type);
            formData.append("post_id", post_id);
            formData.append("account_id", account_id);


            
            var third_url = "<?php echo site_url('Modal_Bodies/post_information/'); ?>"+post_id;

            var page_url = $(location).attr('href').split( '/' );
            var page_number = page_url[ page_url.length - 1 ]; // 2
            var func = page_url[ page_url.length - 2 ]; // projects

            var regexp = new RegExp('#([^\\s]*)','g');
            page_number = page_number.replace(regexp, '');
            
            console.log(page_number);
            var go = new FormData();
            if(func.startsWith("news_page")){
                var decision = "news_page";
            }
            else{
                var decision = "timeline_page";
                
            }
            go.append('result', decision);
            go.append('item_id', post_id);
            var second_url = "<?php echo site_url('Refresh/refresh_post');?>";

            $.ajax({
                url: url_dest,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function(){ $("#loader_animation").show(); },
                success: function(response){
                    console.log(desc);
                    $("span#title"+post_id).text(title);
                    $("span#desc"+post_id).text(desc);
                    $("span#type"+post_id).text(project_type);
                    $('#newsfeed-modal').modal('hide');
                    $("#loader_animation_2").hide();
                    // if(response == 'true'){
                    //     $.ajax({
                    //         url:  second_url,
                    //         type: 'POST',
                    //         processData: false,
                    //         contentType: false,
                    //         cache: false,
                    //         data: go,
                    //         success: function(msg){
                    //             
                    //             
                    //             $("div#contain"+post_id).html(msg);
                    //         }
                    //     });
                    // }
                    // else{
                    //     $("#loader_animation").hide();
                    //     $('#update_post_form_body').html(response);
                    // }
                },
                error: function(){
                    console.log("UH OH! SOMETHING WENT WRONG");
                }
            });
            return false;
        }
    });
   
</script>
</div>