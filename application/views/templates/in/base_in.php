<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title id="tab_title">UpBuilders</title>
    <?php $this->load->view($load_css); ?>

</head>

<body style="overflow-y: hidden;">
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> 
                        <span>
                            <img alt="image" class="img-circle" src="<?php echo base_url(); ?>assets/images/profile_images/profiles/<?php echo $this->session->profile_pic; ?>" 
                            style="width:75px; height:75px;" />
                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> 
                                <span class="block m-t-xs" id="sidemenu_full_name_display"> 
                                    <strong class="font-bold"><?php echo $this->session->full_name; ?></strong>
                                </span> 
                                <span class="text-muted text-xs block">
                                    <?php echo $this->session->privilege; ?><b class="caret"></b>
                                </span> 
                            </span> 
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <?php if($this->session->privilege == 'Client'){  ?>
                            <li><a href="<?php echo base_url('Account/timeline_page'); ?>">Timeline</a></li>
                        <?php }
                        elseif($this->session->privilege == 'Builder'){ ?>
                            <li><a href="<?php echo base_url('Account/builder_timeline_page'); ?>">
                            Timeline</a></li>
                            <li><a href="<?php echo base_url('Account/projects_page'); ?>">Projects</a></li>
                        <?php
                        }
                        ?>
                            <li><a href="<?php echo site_url('Account/profile_settings'); ?>">Profile Settings</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo site_url('UB/logout'); ?>">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        U.B
                    </div>
                </li>
                <li>
                    <a href="<?php echo site_url('Account/landing_page'); ?>"><i class="fa fa-picture-o"></i> 
                        <span class="nav-label">Structural Layouts</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('Account/news_page/1'); ?>"><i class="fa fa-newspaper-o"></i> 
                        <span class="nav-label">News Updates</span>
                    </a>
                </li>
                <li>
                    <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">
                        Mailbox </span><span class="label label-warning pull-right">16/24</span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo site_url('Account/inbox_page'); ?>">Inbox</a></li>
                        <li><a href="mail_compose.html">Compose email</a></li>
                    </ul>
                </li>
        <?php if($this->session->privilege == 'Admin'){ ?>
                <li>
                    <a href="index.html"><i class="fa fa-users"></i> 
                        <span class="nav-label">Accounts</span> <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo site_url('Account/admin_page'); ?>">
                                Administrator
                            </a>
                        </li>
                        <li><a href="">Builder</a></li>
                        <li><a href="<?php echo site_url('Account/client_page'); ?>">Client</a></li>
                    </ul>
                </li>
                <li>
                    <a href="grid_options.html"><i class="fa fa-file-text-o"></i> 
                        <span class="nav-label">Reports</span>
                    </a>
                </li>
        <?php } ?>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg" style="background-color: white;">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" 
                style="margin-bottom: 0">
                    <div class="navbar-header">
                        <button class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                            <i class="fa fa-bars"></i> 
                        </button>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">
                                Welcome To <strong>UpBuilders</strong>
                            </span>
                        </li>
                        <li>
                            <a href="<?php echo site_url('UB/logout'); ?>">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>        
                    </ul>
                </nav>
            </div>
            <div class="wrapper wrapper-content animated fadeInDown" 
            style="background-color: white;height:85.3vh; overflow-y: auto;">
                <div id="base_content" style="background-color: white;">
                    <?php $this->load->view($content); ?>
                </div>
                
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> UpBuilders &copy; 2016-2017
                </div>
            </div>
        </div>
        
    </div>
</body>
</html>
<?php $this->load->view($load_js); ?>
