<div id="post_body">
    <style type="text/css">
        #by{
            font-size:15px; 
            font-weight:bold;
        }
        #indent{
            text-indent: 50px;
        }
    </style>
    <?php 
    foreach($data as $data){
        $post_start_convert = date('h:i a m-d-Y', strtotime($data['post_start']));
        $post_start = explode(' ', $post_start_convert);

        $post_end_convert = date('h:i a m-d-Y', strtotime($data['post_end']));
        $post_end = explode(' ', $post_start_convert);
    ?>
        <h1 style="margin-top:0px; margin-left:-2px; margin-bottom:0px;">
        <b><?php echo $data['title']; ?></b></h1>
        
        <span id="by">By: <?php echo $data['first_name']." ".$data['last_name']; ?> | 
        <?php echo $post_start[2]." at ".$post_start[0].$post_start[1]; ?></span><br>
        
        <?php  
        if($data['email'] == $this->session->email){
        ?>
        <a href="<?php echo site_url('Modal_Bodies/post_update_form/'.$data['id']);?>" 
        id="modal_update_post" class="btn btn-sm btn-warning btn-outline" style="margin-right:5px;
         width:100px; margin-bottom:-10px; margin-top:10px;"> Update</a>

        <a href="<?php echo site_url('Modal_Bodies/delete_post_confirmation/'.$data['id']); ?>" 
        class="btn btn-sm btn-danger btn-outline" id="confirm_delete_post" style="margin-right:5px;
        width: 100px;margin-bottom: -10px; margin-top:10px;"> Delete</a>

        <?php  
        }
        ?>
        <?php 
        if($this->session->privilege == 'Builder'){
            $client_id = $data['account_id'];
            $post_id = $data['id'];
        ?>
            <a href="<?php echo site_url('Registration/builder_request/'.$client_id.'/'.$post_id);?>"
            class="btn btn-sm btn-primary btn-outline" id="apply_button"
            style="margin-right:5px;margin-top:4px;">Apply For This Opportunity</a>
        <?php  
        }
        ?>
        <hr>
        <h5>Post Due Date: <?php echo $post_end[2]; ?></h5>
        <hr>
        <h5><b>Description: </b></h5>
        <p id="indent"><?php echo $data['description']; ?></p>
        <hr>
        <h5>Land Description: </h5>
        <p id="indent"><?php echo $data['land_description']; ?></p>
        <?php  
        foreach($images as $pics){ ?>
            <img src="<?php echo base_url(); ?>assets/images/post_images/<?php echo 
            $pics['file_name']; ?>" style="width:32.5%; height:120px; border-radius: 5px; margin-top:4px;">
        <?php
        }
        ?>
        <br>
        <!-- <a href="<?php echo site_url('Modal_Bodies/post_information/'.$data['id']); ?>" 
        id="cancel_url"></a> -->
        <br>
    <?php
        if($data['email'] == $this->session->email){ ?>
            <h3>Post Members:</h3>
            
                <table class="table">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php  
                    foreach($members as $mem){ ?>
                      <tr>
                        <td><?php echo $mem['first_name']." ".$mem['last_name']; ?></td>
                        <td style="float: right;"><a href="#" class="btn btn-sm btn-warning">Expel Member</a></td>
                      </tr>
                    <?php  
                    }
                    ?>
                    </tbody>
                  </table>
            
            
    <?php
        }
    }
    ?>
    <script type="text/javascript">
        $("a#modal_update_post").click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'modal_update_post'){
                $('h4.modal-title').text('Update Post');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#modal-body').html(msg);
                }
            });
        });
        $("a#confirm_delete_post").click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'confirm_delete_post'){
                $('h4.modal-title').text('Confirm Delete Post');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#confirm-modal').modal('show');
                    $('#confirm-body').html(msg);
                }
            });
        });
        $("a#apply_button").click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'confirm_delete_post'){
                $('h4.modal-title').text('Confirm Delete Post');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    if(msg == 'true'){
                        swal({
                            title: "Good job!",
                            text: "You clicked the button!",
                            type: "success"
                        });
                    }
                    else{
                        swal("Error!", "You have already applied for this post...", "error");
                    }
                }
            });
            
        });
    </script>
</div>