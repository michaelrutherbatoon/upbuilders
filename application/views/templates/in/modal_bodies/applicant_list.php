<table class="table">
	<thead>
	  <tr>
	    <th>Name</th>
	  </tr>
	</thead>
	<tbody>
	<!-- <?php  
		var_dump($data);
	?> -->
	<?php  
	foreach($data as $data){
	?>
		<tr id="tr<?php echo $data['id']; ?>">
	    	<td><?php echo $data['first_name']." ".$data['last_name']; ?></td>
	    	<td>
	    		<div class="sk-spinner sk-spinner-wave" id="loader_animation<?php echo $data['id']?>">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
	    	</td>
	    	<td>
	    		<a href=""
	    		class="btn btn-warning btn-sm"
	    		style="float:right; margin-left: 5px; width:65px;">Decline</a>
	    		<a href="<?php echo site_url('Registration/accept_post_application/'.$data['post_id'].'/'.
	    		$data['client_id'].'/'.$data['builder_id'].'/'.$data['id']); ?>"  class="btn btn-primary btn-sm"
	    		id="accept_proposal_post"
	    		style="float:right; width:65px;">Accept</a>
	    	</td>
	  	</tr>
	<?php  
	}
	?>
	</tbody>
</table>
<script type="text/javascript">
	$('.sk-spinner').hide();
	$("a#accept_proposal_post").click(function(event){
        event.preventDefault(); 
        var myurl = $(this).attr("href");
        var app_id = myurl.substr(myurl.lastIndexOf('/') + 1);
        var id = $(this).attr("id");
        $.ajax({
            url: myurl,
            beforeSend: function(){ $("div#loader_animation"+app_id).show(); },
            success: function(msg) {
                $('.sk-spinner').hide();
                $('tr#tr'+app_id).remove();
            }
        });
    });
</script>