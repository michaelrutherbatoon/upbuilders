<div class="row" style="width:100%;">
    <audio id="notif_audio"><source src="<?php echo base_url('sounds/notify.ogg');?>" type="audio/ogg"><source src="<?php echo base_url('sounds/notify.mp3');?>" type="audio/mpeg"><source src="<?php echo base_url('sounds/notify.wav');?>" type="audio/wav"></audio>
    <!-- <?php  
        var_dump($message);
    ?> -->
    <div class="col-lg-12 animated fadeInRight">
    <div class="mail-box-header">

        <form method="get" action="index.html" class="pull-right mail-search">
            <div class="input-group">
                <input type="text" class="form-control input-sm" name="search" placeholder="Search email">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary">
                        Search
                    </button>
                </div>
            </div>
        </form>
        <h2>
            Inbox (16)
        </h2>
        <div class="mail-tools tooltip-demo m-t-md">
            <div class="btn-group pull-right">
                <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

            </div>
            <a
            class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Refresh inbox">
            <i class="fa fa-refresh"></i></button>
            <a href="<?php echo site_url('Modal_Bodies/message_form'); ?>" id="compose_new" 
            class="btn btn-white btn-sm"><b>Compose New Message</b></a>
            
            <a
            class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Inbox">
            <i class="fa fa-inbox"></i></a>

            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Sent Messages"><i class="fa fa-envelope-o"></i> </button>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i> </button>


        </div>
    </div>
        <div class="mail-box">

        <table class="table table-hover table-mail">
        <tbody id="tbody_body" style="font-weight: bold;">
            <?php  
            if($message != null){
                foreach($message as $mess){
            ?>
            <tr class="unread">
                <td class="check-mail">
                    <input type="checkbox" class="i-checks">
                </td>
                <td class="mail-ontact"><a href="mail_detail.html"><?php echo $mess['from_name']; ?></a></td>
                <td class="mail-subject"><a href="mail_detail.html">Lorem ipsum dolor noretek imit set.</a></td>
                <td class="text-right mail-date">6.10 AM</td>
            </tr>
            <?php  
                }
            }
            ?>
        </tbody>
        </table>


        </div>
    </div>
    <div id="inbox-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
          </div>
          <div class="modal-body" id="modal-body">
            <p>Some text in the modal.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Close</b></button>
          </div>
        </div>

      </div>
    </div>

    <script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
    <script type="text/javascript">
    $('#add_message').hide();
    $("a#compose_new").
        click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'compose_new'){
                $('h4.modal-title').text('Compose New Message');
                $("#loader_animation").hide();
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#inbox-modal').modal('show');
                    $('#modal-body').html(msg);
                }
            });
        });
    
        
    </script>
</div>
