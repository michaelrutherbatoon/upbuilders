<div id="part-body">
<style type="text/css">
    .btn-post-option{
        width:80px; 
        margin-right: 5px; 
        margin-top:4px;
    }
    .btn{
        font-weight: bold;
    }
    #modal-body{
        max-height: 450px;
        overflow-y: auto;
    }
    .modal-header{
        background-color: #0D343C;
        color: white;
    }
    .btn-footer{
        width:90px;
    }
</style>
<div class="row" style="width:100%;">
    <center>
        <span>
            <img alt="image" class="img-circle" style="width:125px; height:125px;" src="<?php 
            echo base_url(); ?>assets/images/profile_images/profiles/<?php echo $this->session->profile_pic; ?>"/>
            <h2><?php echo $this->session->full_name; ?></h2>
        </span>
    </center>
    <br>
    <div class="tabs-container" style="margin-left: 10px;">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1">Posts</a></li>
            <li class=""><a data-toggle="tab" href="#tab-2">Projects</a></li>
        </ul>
        <div class="tab-content"  style="height:100%;">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <?php  
                    foreach($data as $data){
                        $page = "timeline_page";
                        $post_start_convert = date('h:i a m-d-Y', 
                        strtotime($data['post_start']));
                        $post_start = explode(' ', $post_start_convert);
                    ?>
                        <div id="contain<?php echo $data['id']?>" style="width:100%; background-color:#F9F9F9; 
                        border-radius: 5px; border: 1px solid #E7EAEC;
                        padding-left: 10px; padding-right: 10px;">
                            <h2 class="title"><b>
                            <span id="title<?php echo $data['id']; ?>"><?php echo 
                            $data['title']; ?></span></b></h2>

                            <h5><b><span>Posted: <?php echo $post_start[2]." at ".$post_start[0]."".$post_start[1]; ?> | </span>
                            <span id="type<?php echo $data['id']; ?>">
                            <?php echo $data['type']; ?></span></b></h5>


                            <select id="option_picker" class="form-control" style="width:20%;
                            font-weight: bold;">
                                <option>Option</option>
                                <option value="<?php echo site_url('Modal_Bodies/post_information/'.$data['id']); ?>" func="view_post">View Post Info</option>

                                <option value="<?php echo site_url('Modal_Bodies/view_post_applicant/'
                                .$data['id'].'/'.$page); ?>" func="view_app">View
                                Applicants</option>

                                <option value="<?php echo site_url('Modal_Bodies/post_update_form/'
                                .$data['id']); ?>" func="update"> Update Post </option>
                                <option value="<?php echo site_url('Modal_Bodies/delete_post_confirmation/'
                                .$data['id'].'/'.$page); ?>" func="delete" > Delete Post </option>
                            </select><br>
                            

                            <br><br>
                            <h5>Description: </h5>
                            <p style="text-indent: 50px;">
                            <span id="desc<?php echo $data['id']; ?>"><?php echo 
                            $data['description']; ?></span></p>
                        </div><br>
                    <?php  
                    }
                    ?>

                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <?php  
                        var_dump($data);
                    ?>
                </div>
            </div>
        </div>


    </div>
</div>

      <!-- Modal -->
<div class="modal fade" id="newsfeed-modal" role="dialog">
    <div class="modal-dialog">
  <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body" id="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-footer" data-dismiss="modal">Close</button>
                
            </div>
        </div>
  
    </div>
</div>

<div class="modal" id="confirm-modal" role="dialog">
    <div class="modal-dialog modal-sm" style="margin-top:150px;">
        <div class="modal-content">
            <div class="modal-body" id="confirm-body"></div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script type="text/javascript">
    $('select#option_picker').change(function() {
    // set the window's location property to the value of the option the user has selected
        var myurl = $(this).val();
        var text_option = $(this).find('option:selected').attr('func');

        event.preventDefault(); 
        var id = $(this).attr("id");
        if(id == 'update_post'){
            $('h4.modal-title').text('Update Post');
            $("#loader_animation").hide();
        }
        if(id == 'post_new_button'){
            $('a#newsfeed-cancel').hide();
            $('h4.modal-title').text('New Post');
        }
        if(id == 'view_post_info'){
            $('a#newsfeed-cancel').hide();
            $('h4.modal-title').text('Post Information');
        }
        $.ajax({
            url: myurl,
            success: function(msg) {
                $("select#option_picker").val($("select#option_picker option:first").val());
                if(text_option == 'delete'){
                    $('#confirm-modal').modal('show');
                    $('#confirm-body').html(msg);
                }
                else{
                    $('#newsfeed-modal').modal('show');
                    $('#modal-body').html(msg);
                }
                
            }
        });
    });
    $( document ).ready(function() {
        $("#loader_animation").hide();
        $('#vertical-timeline').toggleClass('center-orientation');

        $("a#update_post, a#post_new_button, a#view_post_info, a#newsfeed-cancel, a#view_applicants").
        click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'update_post'){
                $('h4.modal-title').text('Update Post');
                $("#loader_animation").hide();
            }
            if(id == 'post_new_button'){
                $('a#newsfeed-cancel').hide();
                $('h4.modal-title').text('New Post');
            }
            if(id == 'view_post_info'){
                $('a#newsfeed-cancel').hide();
                $('h4.modal-title').text('Post Information');
            }
            if(id == 'view_applicants'){
                $('a#newsfeed-cancel').hide();
                $('h4.modal-title').text('Applicants List');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#newsfeed-modal').modal('show');
                    $('#modal-body').html(msg);
                }
            });
        });
        $("a#confirm_delete_post").click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'confirm_delete_post'){
                $('h4.modal-title').text('Confirm Delete Post');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#confirm-modal').modal('show');
                    $('#confirm-body').html(msg);
                }
            });
        });

    });
    
</script>
</div>
<!-- <div class="modal inmodal" id="post_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button class="btn" id="modal_back_button" style="float:left; border:0px; 
                background-color: white; margin-right: -60px;">
                    <span class="fa fa-chevron-left" style="font-size: 30px;"></span>
                </button>
                <center>
                    <h4 class="modal-title" id="modal_title">Modal title</h4>
                </center>
                
            </div>
            <div class="modal-body" id="body_post_modal">
            </div>
            <div class="modal-footer">
                <button type="button" class="close-modal btn btn-danger" data-dismiss="modal"
                style="font-weight:bold;">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("a#view_post_info_timeline")
            .click(function(event){
                event.preventDefault(); 
                var myurl = $(this).attr("href");
                var id = $(this).attr("id");
                if(id == 'view_post_info_timeline'){
                    $('h4#modal_title').text('Post Information');
                }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#post_modal').modal('show');
                    $('#body_post_modal').html(msg);
                    $("h3#by").hide();
                }
            });
        });
        $("a#update_post").click(function(event){
              event.preventDefault(); 
              var myurl = $(this).attr("href");
              var id = $(this).attr("id");
              if(id == 'update_post'){
                $('h4#modal_title').text('Update Post');
              }
          $.ajax({
              url: myurl,
              success: function(msg) {
                $("button#modal_back_button").hide();
                $('#post_modal').modal('show');
                $('#body_post_modal').html(msg);
              }
            });
          });
    });
</script> -->