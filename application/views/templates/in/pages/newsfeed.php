<div id="part-body">
    <style type="text/css">
        .btn-post-option{
            width:80px; 
            margin-right: 5px; 
            margin-top:4px;
        }
        .btn{
            font-weight: bold;
        }
        #modal-body{
            max-height: 450px;
            overflow-y: auto;
        }
        .btn-footer{
            width:90px;
        }
    </style>
    <a href="<?php echo site_url('Modal_Bodies/add_new_post_form'); ?>" class="btn btn-success" 
     id="post_new_button">Click Here To Post...</a>
    <a href="" class="btn btn-info">Filter Posts Here...</a>
    <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
        <div id="first_alwasy"></div>

        <?php
        if($data != null){
        $i = 0;
        foreach($data as $data){
            $id[$i] = $data['id'];
            $i++;

        ?>
            <div class="vertical-timeline-block" id="contain<?php echo $data['id'];?>">
                <?php  
                    $post_start_convert = date('h:i a m-d-Y', strtotime($data['post_start']));
                    $post_start = explode(' ', $post_start_convert);
                ?>

                <div class="vertical-timeline-icon navy-bg">
                    <i class="fa fa-building"></i>
                </div>
                <div class="vertical-timeline-content">
                    <h2><b><span id="title<?php echo $data['id'];?>"><?php echo $data['title']; ?>
                    </span></b> | <span style="font-size:15px; font-weight:bold;"> By: <?php echo 
                    $data['first_name']." ".$data['last_name']; ?></span></h2>

                    <h4 class="vertical-date" style="margin-top:-5px; font-size: bold;">
                    <?php echo $post_start[2]." at ".$post_start[0].$post_start[1]; ?> | 
                    <span id="type<?php echo $data['id']?>"><?php echo $data['type']; ?></span></h4>
                    <br>
                    <p style="text-indent: 50px;"><span id="desc<?php echo $data['id'];?>"><?php echo 
                    $data['description']; ?></span></p>
                    
                    
                    

                    <a href="<?php echo site_url('Modal_Bodies/post_information/'.$data['id']); ?>" 
                    id="view_post_info" class="btn btn-sm btn-default btn-outline btn-post-option" 
                    style=""> 
                    More Info</a>
                    <?php  
                    if($this->session->email == $data['email']){
                    ?>
                        <a href="<?php echo site_url('Modal_Bodies/post_update_form/'.
                        $data['id']);?>" id="update_post" class="btn btn-sm btn-warning 
                        btn-outline btn-post-option">Update</a>
                        
                        <a href="<?php echo site_url('Modal_Bodies/delete_post_confirmation/'.$data['id']); ?>" 
                        class="btn btn-sm btn-danger btn-outline btn-post-option"
                        id="confirm_delete_post"> Delete</a>
                    <?php  
                    }
                    ?>
                </div>
            </div>
        <?php  
        }
            $last_id = $id[0];
        }
        ?>
    </div>
      <!-- Modal -->
    <div class="modal fade" id="newsfeed-modal" role="dialog">
        <div class="modal-dialog">
      <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" id="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body" id="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-footer" data-dismiss="modal">Close</button>
                    
                </div>
            </div>
      
        </div>
    </div>

    <div class="modal" id="confirm-modal" role="dialog">
        <div class="modal-dialog modal-sm" style="margin-top:150px;">
            <div class="modal-content">
                <div class="modal-body" id="confirm-body"></div>
            </div>
        </div>
    </div>


<script type="text/javascript">
    $( document ).ready(function() {
        $("#loader_animation").hide();
        $('#vertical-timeline').toggleClass('center-orientation');

        $("a#update_post, a#post_new_button, a#view_post_info").
        click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'update_post'){
                $('h4.modal-title').text('Update Post');
                $("#loader_animation").hide();
            }
            if(id == 'post_new_button'){
                $('#newsfeed-cancel').hide();
                $('h4.modal-title').text('New Post');
            }
            if(id == 'view_post_info'){
                $('#newsfeed-cancel').hide();
                $('h4.modal-title').text('Post Information');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#newsfeed-modal').modal('show');
                    $('#modal-body').html(msg);
                }
            });
        });

        
        $("a#confirm_delete_post").click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#confirm-modal').modal('show');
                    $('#confirm-body').html(msg);
                }
            });
        });

    });
    
</script>
</div>



                        