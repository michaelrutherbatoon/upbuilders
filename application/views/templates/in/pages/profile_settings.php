<div style="margin-top:20px;">
  <style type="text/css">
    .btn{
      font-weight: bold;
    }
  </style>
  <img src="<?php echo base_url(); ?>assets/images/profile_images/profiles/<?php echo 
  $this->session->profile_pic; ?>" id="profile_page_image"/>

  <center>
    <table class="table table-striped table-bordered table-hover">
      <tbody>
        <?php 

         ?>
        <tr>
          <th style="border:0px;">Username</th>
          <td style="border:0px;"><?php echo $this->session->username; ?></td>
          <td style="border:0px;"></td>
        </tr>
        <tr>
          <th style="border:0px;">Password</th>
          <td style="border:0px;">***************</td>
          <td style="float:right;border:0px; font-weight: bold;"><a href="<?php echo 
          site_url('Update/password_form'); ?>" id="update_password">EDIT</a></td>
        </tr>
        <tr>
          <th style="border:0px;">Full Name</th>
          <td style="border:0px;" id="full_name_display"><?php echo $this->session->full_name; ?></td>
          <td style="float:right;border:0px; font-weight: bold;"><a href="<?php echo 
          site_url('Update/fullname_form'); ?>" id="update_fullname">EDIT</a></td>
        </tr>
        <tr>
          <th style="border:0px;">Birth Date</th>
          <td style="border:0px;" id="birth_date_display">
          <?php echo $this->session->birth_date; ?></td>
          <td style="float:right;border:0px; font-weight: bold;"><a href="<?php echo 
          site_url('Update/birthdate_form'); ?>" id="update_birthdate">EDIT</a></td>
        </tr>
        <tr>
          <th style="border:0px;">Email Address</th>
          <td style="border:0px;" id="email_display"><?php echo $this->session->email; ?></td>
          <td style="float:right;border:0px; font-weight: bold;"><a href="<?php echo 
          site_url('Update/email_form'); ?>" id="update_email">EDIT</a></td>
        </tr>
        <tr>
          <th style="border:0px;">Contact Number</th>
          <td style="border:0px;" id="contact_display">+63<?php echo $this->session->contact; ?></td>
          <td style="float:right;border:0px; font-weight: bold;"><a href="<?php echo 
          site_url('Update/contact_form'); ?>" id="update_contact">EDIT</a></td>
        </tr>
      <?php 
      if($this->session->privilege == 'Client'){ 
      ?>
        <tr>
          <th style="border:0px;">Occupation</th>
          <td style="border:0px;" id="occupation_display"><?php echo $this->session->occupation; ?> 
          </td>
          <td style="float:right;border:0px; font-weight: bold;"><a href="<?php echo 
          site_url('Update/occupation_form'); ?>" id="update_occupation">EDIT</a></td>
        </tr>
        <tr>
          <th style="border:0px;">Work Address</th>
          <td style="border:0px;" id="workaddress_display"><?php echo $this->session->work_address; ?></td>
          <td style="float:right;border:0px; font-weight: bold;"><a href="<?php echo 
          site_url('Update/workaddress_form'); ?>" id="update_workaddress">EDIT</a></td>
        </tr>
      <?php 
      }
      elseif($this->session->privilege == 'Builder'){
      ?>
        <tr>
          <th style="border:0px;">Degree</th>
          <td style="border:0px;" id="degree_display"><?php echo $this->session->degree; ?></td>
          <td style="float:right;border:0px; font-weight: bold;">
            <a href="<?php echo site_url('Update/degree_form'); ?>" id="update_degree">EDIT</a>
          </td>
        </tr>
        <tr>
          <th style="border:0px;">Firm</th>
          <td style="border:0px;" id="firm_display"><?php echo $this->session->firm; ?></td>
          <td style="float:right;border:0px; font-weight: bold;">
            <a href="<?php echo site_url('Update/firm_form'); ?>" id="update_firm">EDIT</a>
          </td>
        </tr>
      <?php 
      } 
      ?>
      </tbody>
    </table>
  </center>
  <div class="modal fade" id="profile-settings-modal" role="dialog">
      <div class="modal-dialog">
    <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Modal Header</h4>
              </div>
              <div class="modal-body" id="modal-body">
                  <p>Some text in the modal.</p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-footer" data-dismiss="modal">Close</button>
                  
              </div>
          </div>
      </div>
  </div>
  <div class="modal" id="confirm-modal" role="dialog">
      <div class="modal-dialog modal-sm" style="margin-top:150px;">
          <div class="modal-content">
              <div class="modal-body" id="confirm-body"></div>
          </div>
      </div>
  </div>
  <script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $("a#update_password,a#update_fullname,a#update_birthdate,#update_email,a#update_contact, a#update_occupation,a#update_workaddress,a#update_degree,a#update_firm")
        .click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'update_password'){
              $('h4.modal-title').text('Update Password');
            }
            else if(id == 'update_fullname'){
              $('h4.modal-title').text('Update Full Name');
            }
            else if(id == 'update_birthdate'){
              $('h4.modal-title').text('Update Birth Date');
            }
            else if(id == 'update_email'){
              $('h4.modal-title').text('Update Email Address');
            }
            else if(id == 'update_contact'){
              $('h4.modal-title').text('Update Contact Number');
            }
            else if(id == 'update_occupation'){
              $('h4.modal-title').text('Update Occupation');
            }
            else if(id == 'update_workaddress'){
              $('h4.modal-title').text('Update Work Address');
            }
            else if(id == 'update_degree'){
              $('h4.modal-title').text('Update Degree');
            }
            else if(id == 'update_firm'){
              $('h4.modal-title').text('Update Firm');
            }
        $.ajax({
            url: myurl,
            success: function(msg) {
              $('#profile-settings-modal').modal('show');
              $('#modal-body').html(msg);
            }
          });
        });
    });
    </script>
</div>
