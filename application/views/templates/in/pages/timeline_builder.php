<div id="timeline_page">
<style type="text/css">
    .btn-post-option{
        width:80px; 
        margin-right: 5px; 
        margin-top:4px;
    }
    .btn{
        font-weight: bold;
    }
    #modal-body{
        max-height: 450px;
        overflow-y: auto;
    }
    .modal-header{
        background-color: #0D343C;
        color: white;
    }
    .btn-footer{
        width:90px;
    }
</style>
<div class="row" style="width:100%;">
    <center>
        <span>
            <img alt="image" class="img-circle" style="width:125px; height:125px;" src="<?php 
            echo base_url(); ?>assets/images/profile_images/profiles/<?php echo $this->session->profile_pic; ?>"/>
            <h2><?php echo $this->session->full_name; ?></h2>
        </span>
    </center>
    <br>
    <div class="tabs-container" style="margin-left: 10px;">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1">Applications</a>
            </li>
            <li class=""><a data-toggle="tab" href="#tab-2">Projects</a></li>
            <li class=""><a data-toggle="tab" href="#tab-3">Proposals</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4">Designs</a></li>
        </ul>
        <div class="tab-content"  style="height:100%;">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <?php  
                    foreach($data as $data){
                        $page = "timeline_page";
                        $post_start_convert = date('h:i a m-d-Y', 
                        strtotime($data['post_start']));
                        $post_start = explode(' ', $post_start_convert);
                    ?>
                        <div style="width:100%; background-color:#F9F9F9; 
                        border-radius: 5px; border: 1px solid #E7EAEC;
                        padding-left: 10px; padding-right: 10px;">
                            <h2 class="title"><b><?php echo $data['title']; ?></b>
                            </h2>
                            <h5><b><span>Posted: <?php echo $post_start[2]." at ".$post_start[0]."".$post_start[1]; ?> | </span><?php echo $data['type']; ?></b></h5>
                            
                            <select id="option_picker" class="form-control" style="width:20%;font-weight: bold;">
                                <option>Option</option>
                                <option value="<?php echo site_url('Modal_Bodies/post_information/'.
                                $data['id']); ?>" func="view_post">View Post Info</option>
                                <option value="<?php echo site_url('Modal_Bodies/cancel_app_confirmation/'
                                .$data['id']); ?>" func="cancel">Cancel Application</option>
                            </select><br>

                            <!-- <a href="<?php echo site_url('Modal_Bodies/post_information/'.$data['id']); ?>" 
                            id="view_post_info" class="btn btn-sm btn-primary btn-outline"
                            style="width:140px; font-weight: bold;">Full Info.
                            </a>
                            <a href="<?php echo 
                            site_url('Modal_Bodies/cancel_app_confirmation/'.$data['id']); ?>" 
                            class="btn btn-sm btn-warning btn-outline" 
                            id="cancel_button"
                            style="width:140px; font-weight: bold;">Cancel Application</a> -->

                            <h5>Description: </h5>
                            <p style="text-indent: 50px;"><?php echo 
                            $data['description']; ?></p>
                        </div><br>
                    <?php  
                    }
                    ?>

                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <?php  
                        var_dump($data);
                    ?>
                </div>
            </div>
            <div id="tab-3" class="tab-pane">
                <div class="panel-body">
                    <?php  
                    foreach($proposal as $proposal){
                        $page = "timeline_page";
                        $post_start_convert = date('h:i a m-d-Y', 
                        strtotime($proposal['post_start']));
                        $post_start = explode(' ', $post_start_convert);
                    ?>
                        <div style="width:100%; background-color:#F9F9F9; 
                        border-radius: 5px; border: 1px solid #E7EAEC;
                        padding-left: 10px; padding-right: 10px;">
                            <h2 class="title"><b><?php echo $proposal['title']; ?></b>
                            </h2>
                            <h5><b><span>Posted: <?php echo $post_start[2]." at ".$post_start[0]
                            ."".$post_start[1]; ?> | </span><?php echo $proposal['type']; ?></b></h5>
                            <select id="option_picker" class="form-control" style="width:20%;font-weight: bold;">
                                <option>Option</option>
                                <option value="<?php echo site_url('Modal_Bodies/post_information/'.
                                $proposal['id']); ?>" func="view_post">View Post Info</option>
                                <option value="<?php echo site_url('Modal_Bodies/cancel_app_confirmation/'
                                .$proposal['id']); ?>" func="leave">Leave Post</option>
                                <option>Pass Proposal</option>
                            </select><br>

                            <br><br>
                            <h5>Description: </h5>
                            <p style="text-indent: 50px;"><?php echo 
                            $proposal['description']; ?></p>
                        </div><br>
                    <?php  
                    }
                    ?>
                </div>
            </div>
            <div id="tab-4" class="tab-pane">
                <div class="panel-body">
                    <h1>My Designs</h1>
                </div>
            </div>
        </div>
    </div>
</div>

      <!-- Modal -->
<div class="modal fade" id="newsfeed-modal" role="dialog">
    <div class="modal-dialog">
  <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body" id="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-footer" data-dismiss="modal">Close
                </button>
                
            </div>
        </div>
  
    </div>
</div>

<div class="modal" id="confirm-modal" role="dialog">
    <div class="modal-dialog modal-sm" style="margin-top:150px;">
        <div class="modal-content">
            <div class="modal-body" id="confirm-body"></div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script type="text/javascript">
    $('select#option_picker').change(function() {
    // set the window's location property to the value of the option the user has selected
        var text_option = $(this).find('option:selected').attr('func');
        console.log(text_option);
        var myurl = $(this).val();
        event.preventDefault(); 
        var id = $(this).attr("id");
        if(id == 'update_post'){
            $('h4.modal-title').text('Update Post');
            $("#loader_animation").hide();
        }
        if(id == 'post_new_button'){
            $('a#newsfeed-cancel').hide();
            $('h4.modal-title').text('New Post');
        }
        if(id == 'view_post_info'){
            $('a#newsfeed-cancel').hide();
            $('h4.modal-title').text('Post Information');
        }
        $.ajax({
            url: myurl,
            success: function(msg) {
                $("select#option_picker").val($("select#option_picker option:first").val());
                if(text_option == 'cancel' || text_option == 'leave'){
                    $('#confirm-modal').modal('show');
                    $('#confirm-body').html(msg);
                }
                else{
                    $('#newsfeed-modal').modal('show');
                    $('#modal-body').html(msg);
                }
                
            }
        });
    });
    $( document ).ready(function() {
        $("#loader_animation").hide();
        $('a#newsfeed-cancel').hide();
        $('#vertical-timeline').toggleClass('center-orientation');

        $("a#update_post, a#post_new_button, a#view_post_info, a#newsfeed-cancel").
        click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'update_post'){
                $('h4.modal-title').text('Update Post');
                $("#loader_animation").hide();
            }
            if(id == 'post_new_button'){
                $('a#newsfeed-cancel').hide();
                $('h4.modal-title').text('New Post');
            }
            if(id == 'view_post_info'){
                $('a#newsfeed-cancel').hide();
                $('h4.modal-title').text('Post Information');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#newsfeed-modal').modal('show');
                    $('#modal-body').html(msg);
                }
            });
        });
        $("a#cancel_button").click(function(event){
            event.preventDefault(); 
            var myurl = $(this).attr("href");
            var id = $(this).attr("id");
            if(id == 'confirm_delete_post'){
                $('h4.modal-title').text('Confirm Delete Post');
            }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#confirm-modal').modal('show');
                    $('#confirm-body').html(msg);
                }
            });
        });

    });
    
</script>
</div>
<script type="text/javascript">
    
</script>
<!-- <div class="modal inmodal" id="post_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button class="btn" id="modal_back_button" style="float:left; border:0px; 
                background-color: white; margin-right: -60px;">
                    <span class="fa fa-chevron-left" style="font-size: 30px;"></span>
                </button>
                <center>
                    <h4 class="modal-title" id="modal_title">Modal title</h4>
                </center>
                
            </div>
            <div class="modal-body" id="body_post_modal">
            </div>
            <div class="modal-footer">
                <button type="button" class="close-modal btn btn-danger" data-dismiss="modal"
                style="font-weight:bold;">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("a#view_post_info_timeline")
            .click(function(event){
                event.preventDefault(); 
                var myurl = $(this).attr("href");
                var id = $(this).attr("id");
                if(id == 'view_post_info_timeline'){
                    $('h4#modal_title').text('Post Information');
                }
            $.ajax({
                url: myurl,
                success: function(msg) {
                    $('#post_modal').modal('show');
                    $('#body_post_modal').html(msg);
                    $("h3#by").hide();
                }
            });
        });
        $("a#update_post").click(function(event){
              event.preventDefault(); 
              var myurl = $(this).attr("href");
              var id = $(this).attr("id");
              if(id == 'update_post'){
                $('h4#modal_title').text('Update Post');
              }
          $.ajax({
              url: myurl,
              success: function(msg) {
                $("button#modal_back_button").hide();
                $('#post_modal').modal('show');
                $('#body_post_modal').html(msg);
              }
            });
          });
    });
</script> -->