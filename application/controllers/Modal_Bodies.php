<?php
class Modal_Bodies extends CI_Controller {

	public function post_information($post_id){
		$temp_data['images'] = $this->Model_Privileges->fetch_post_file($post_id);
		$temp_data['data'] = $this->Model_Privileges->fetch_post_info($post_id);
		$temp_data['members'] = $this->Model_Privileges->fetch_post_members($post_id);
		$this->load->view('templates/in/modal_bodies/post_info', $temp_data);	
	}
	public function admin_registrationForm(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				$this->load->view('templates/admin/admin/admin_registration_form');
			}
			else{
				$dest = site_url('Account/landing_page');
				echo "<script>window.location.href='$dest'</script>";
			}
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}
	public function confirm_change_status_message($id){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				$temp_data['id'] = $id;
				$this->load->view('templates/messages/templates/change_account_status_confirmation', $temp_data);
			}
			else{
				$dest = site_url('Account/landing_page');
				echo "<script>window.location.href='$dest'</script>";
			}
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}	
	}
	public function confirm_reset_message($id){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				$temp_data['id'] = $id;
				$this->load->view('templates/messages/templates/reset_account_confirmation', $temp_data);
			}
			else{
				$dest = site_url('Account/landing_page');
				echo "<script>window.location.href='$dest'</script>";
			}
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}
	public function account_info($id){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$user_data['account_data'] = $this->Model_Accounts->select_account($id);
			foreach($user_data['account_data'] as $cont){
				$privilege = $cont['privilege'];
			}
			$user_data['profile_data'] = $this->Model_Accounts->select_profile($id);
			if($privilege == 'Client'){
				$user_data['client_data'] = $this->Model_Accounts->select_client($id);
			}
			elseif($privilege == 'Builder'){
				$user_data['builder_data'] = $this->Model_Accounts->select_builder($id);
			}
			$this->load->view('templates/admin/user_info', $user_data);
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}
	public function view_post_applicant($post_id){
		$temp_data['data'] = $this->Model_Privileges->view_applicants($post_id);
		$this->load->view('templates/in/modal_bodies/applicant_list', $temp_data);
	}

	public function message_form(){
		$this->load->view('templates/in/forms/message_forms/new_message');
	}
	


	// POST CRUD FORMS
	public function add_new_post_form(){
		$email = $this->session->email;
		$sql_1 = $this->db->query("SELECT id FROM user_account WHERE email = '$email'")->result_array();
		foreach($sql_1 as $data){
			$account_id = $data['id'];
		}
		$count = $this->db->query("SELECT id FROM client_post WHERE account_id = $account_id")->num_rows();
		if($count < 3){
			$this->load->view('templates/in/forms/post_forms/news_form');
		}
		else{
			echo "<script>$('h4.modal-title').text('Notification');</script>";
			echo "<h2><b>YOU HAVE EXCEED TO YOUR LIMIT! LIMIT IS ONLY 3 POSTS PER USER. DELETE OTHER POSTS 
			YOU HAVE</b></h2>";
		}		

		
	}

	public function post_update_form($id){
		
		$result = $this->Model_Privileges->fetch_post_info($id);
		foreach($result as $data){
			// $result['id'] = ; 
		 //    $result['account_id'];
		 //    $result['title'] = set_value('title');
		 //    $result['type'];
		 //    $result['description'];
		 //    $result['land_description'];
		}
		$post_end = $data['post_end'];
        list($date, $time) = explode(" ", $post_end, 2);
        $data['post_end'] = $date;
		$temp_data['data'] = $data;

		$this->load->view('templates/in/forms/post_forms/update_post', $temp_data);
	}
	public function delete_post_confirmation($post_id){
		$temp_data['id'] = $post_id;
		$this->load->view("templates/messages/confirmations/delete_client_post", $temp_data);
	}
	public function cancel_app_confirmation($post_id){
		$temp_data['id'] = $post_id;
		$this->load->view("templates/messages/confirmations/cancel_application_builder", $temp_data);
	}
}