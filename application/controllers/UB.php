<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UB extends CI_Controller {

	function index(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$dest = site_url('Account/landing_page');
			redirect($dest);
		}
		else{
			// 
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/global_out/landing_page';
			$this->load->view('templates/global_out/base_body_out', $temp_data);
		}
		
	}
	
	function login(){
		if(isset($this->session->password_updated)){
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/global_out/login_page';
			$this->load->view('templates/global_out/base_body_out', $temp_data);
			$this->load->view('templates/messages/modal/password_updated');
			$this->load->view('templates/assets/form_scripts/login_script');
			$this->session->sess_destroy();
		}
		elseif(isset($this->session->username) AND isset($this->session->privilege)){
			$dest = site_url('Account/landing_page');
			redirect($dest);
		}
		else{
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js';
			$temp_data['content'] = 'templates/global_out/login_page';
			$this->load->view('templates/global_out/base_body_out', $temp_data);
			$this->load->view('templates/assets/form_scripts/login_script');
		}
	}
	function verifyLogin(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$dest = site_url('Account/landing_page');
			redirect($dest);
		}
		else{
			if($this->input->post('username') AND $this->input->post('password')){
				$username = $this->security->xss_clean($this->input->post('username'));
				$password = $this->security->xss_clean($this->input->post('password'));
				$loginData = $this->Model_Login->loginVerify($username,$password);
				if($loginData == false){
					echo "false";
					// echo "<script>$(".'".alert"'.").show();</script>";
				}
				else{
					$dest = site_url('Account/landing_page');
					echo $dest;
				}
			}
			else{
				$dest = site_url('UB/login');
				echo "<script>window.location.href='$dest'</script>";
			}	
		}
	}
	function access_failed(){
		$this->load->view('templates/global_out/login_page');
		$this->load->view('templates/assets/form_scripts/login_script');
	}
	function client_registration_form(){
		$temp_data['load_css'] = 'templates/assets/load_css';
		$temp_data['load_js'] = 'templates/assets/load_js'; 
		$temp_data['content'] = 'templates/global_out/registration_page_client';
		$this->load->view('templates/global_out/base_body_out', $temp_data);
		$this->load->view('templates/assets/form_scripts/client_registration_script');
	}

	function builder_registration_form(){
		$temp_data['load_css'] = 'templates/assets/load_css';
		$temp_data['load_js'] = 'templates/assets/load_js'; 
		$temp_data['content'] = 'templates/global_out/registration_page_builder';
		$this->load->view('templates/global_out/base_body_out', $temp_data);
	}
	function logout(){
		$this->session->sess_destroy();
		$dest = site_url('UB/login');
		redirect($dest);
	}
}