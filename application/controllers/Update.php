<?php

class Update extends MY_Controller {

	//PAGE AREA
	public function password_form(){
		$this->load->view('templates/in/forms/profile_forms/password');
		$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
	}
	public function fullname_form(){
		$this->load->view('templates/in/forms/profile_forms/full_name');
		$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
	}
	public function birthdate_form(){
		$this->load->view('templates/in/forms/profile_forms/birth_date');
		$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
	}
	public function email_form(){
		$this->load->view('templates/in/forms/profile_forms/email');
		$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
	}
	public function contact_form(){
		$this->load->view('templates/in/forms/profile_forms/contact');
		$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
	}
	public function occupation_form(){
		$this->load->view('templates/in/forms/profile_forms/occupation');
		$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
	}
	public function workaddress_form(){
		$this->load->view('templates/in/forms/profile_forms/work_address');
		$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
	}
	public function degree_form(){
		echo "THIS IS DEGREE FORM!";
	}
	public function firm_form(){
		echo "THIS IS FIRM FORM";
	}

//
	// SUBMIT AREA
	public function password_submit(){
		$this->form_validation->set_rules('old_password', '"Old Password"', 
		'required|callback_oldpassword_check');
		$this->form_validation->set_rules('new_password', '"New Password"', 
		'required|callback_password_check');
		$this->form_validation->set_rules('password_confirmation', '"Confirm New Password"', 
		'required|matches[new_password]');
		if ($this->form_validation->run() == FALSE){
            $this->load->view('templates/in/forms/profile_forms/password');
			$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
        }
        else{
        	$new_pass = $this->input->post('new_password');
        	$this->Model_Update->update_password($new_pass);
        	$session_data = array('password_updated' => true);
        	$this->session->set_userdata($session_data);
        	echo "password_updated";
        }
	}

	public function fullname_submit(){
		$this->form_validation->set_rules('first_name', '"First Name"', 
		'required|callback_name_check');
		$this->form_validation->set_rules('last_name', '"Last Name"', 
		'required|callback_name_check');
		if ($this->form_validation->run() == FALSE){
            $this->load->view('templates/in/forms/profile_forms/full_name');
			$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
        }
        else{
        	$fname = $this->input->post('first_name');
			$lname = $this->input->post('last_name');
			$this->Model_Update->update_fullname($fname,$lname);
			$full_name = $fname." ".$lname;
			$session_data = array('full_name' => $full_name);
			$this->session->unset_userdata('full_name');
			$this->session->set_userdata($session_data);
			echo "name_updated";
        }
	}
	public function birthdate_submit(){
		$this->form_validation->set_rules('birth_date', '"Birth Date"', 
		'required|callback_date_check');
		if ($this->form_validation->run() == FALSE){
            $this->load->view('templates/global_in/forms/profile_forms/birth_date');
			$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
        }
        else{
        	$bdate = $this->input->post('birth_date');
        	$this->Model_Update->update_birthdate($bdate);
        	$session_data = array('birth_date' => $bdate);
			$this->session->unset_userdata('birth_date');
			$this->session->set_userdata($session_data);
			echo "bdate_updated";
        }
	}
	public function email_submit(){
		
		$this->form_validation->set_rules('email', '"Email Address"', 
		'required|valid_email|is_unique[user_account.email]|callback_email_check');
		if ($this->form_validation->run() == FALSE){
            $this->load->view('templates/global_in/forms/profile_forms/email');
			$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
        }
        else{
        	$email = $this->input->post('email');
        	$this->Model_Update->update_email($email);
        	$session_data = array('email' => $email);
			$this->session->unset_userdata('email');
			$this->session->set_userdata($session_data);
			echo "email_updated";
        }
	}
	public function contact_submit(){
		$this->form_validation->set_rules('contact', '"Contact Number"', 
		'required|callback_contact_check');
		if ($this->form_validation->run() == FALSE){
            $this->load->view('templates/global_in/forms/profile_forms/contact');
			$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
        }
        else{
        	$contact = $this->input->post('contact');
        	$this->Model_Update->update_contact($contact);
        	$session_data = array('contact' => $contact);
			$this->session->unset_userdata('contact');
			$this->session->set_userdata($session_data);
			echo "contact_updated";

        }
	}
	public function occupation_submit(){
		$this->form_validation->set_rules('occupation', '"Occupation"', 
		'required|callback_xss_check');
		if ($this->form_validation->run() == FALSE){
            $this->load->view('templates/global_in/forms/profile_forms/occupation');
			$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
        }
        else{
        	$occupation = $this->security->xss_clean($this->input->post('occupation'));
        	$this->Model_Update->update_occupation($occupation);
        	$session_data = array('occupation' => $occupation);
			$this->session->unset_userdata('occupation');
			$this->session->set_userdata($session_data);
			echo "occupation_updated";
        }
	}
	public function workaddress_submit(){
		
		$this->form_validation->set_rules('work_address', '"Work Address"', 
		'required|callback_address_check');
		if ($this->form_validation->run() == FALSE){
            $this->load->view('templates/global_in/forms/profile_forms/work_address');
			$this->load->view('templates/assets/form_scripts/update_profile_submit_script');
        }
        else{
        	$work_address = $this->security->xss_clean($this->input->post('work_address'));
        	$this->Model_Update->update_workaddress($work_address);
        	$session_data = array('work_address' => $work_address);
			$this->session->unset_userdata('work_address');
			$this->session->set_userdata($session_data);
			echo "work_updated";
        }
	}
	public function confirm_change_status_response(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin' AND $this->input->post('account_id') != null){
				$id = $this->security->xss_clean($this->input->post('account_id'));
				$this->Model_Accounts->change_account_status($id);
				$priv = $this->Model_Accounts->check_privilege($id);
				if($priv == 'Admin'){
					echo "Admin";
				}
				elseif($priv == 'Client'){
					echo "Client";
				}
				elseif($priv == 'Builder'){

				}
				
			}
			else{
				$dest = site_url('Account/landing_page');
				header('location: '.$dest);
			}
		}
		else{
			$dest = base_url();
			header('location: '.$dest);
		}
	}
	public function confirm_reset_response(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				$id = $this->security->xss_clean($this->input->post('account_id'));
				$priv = $this->Model_Accounts->check_privilege($id);
				$this->Model_Accounts->reset_account($id);
				echo "success";
			}
			else{
				$dest = site_url('Account/landing_page');
				header('location: '.$dest);
			}
		}
		else{
			$dest = base_url();
			header('location: '.$dest);
		}
	}
	public function delete_post_response(){
		$post_id = $this->input->post('post_id');

		$config['upload_path'] = "./assets/images/post_images";
		$config['allowed_types'] = 'jpg|png';
		$this->load->library('upload');
		$counter = 0;
		$fetched_prev_data = $this->Model_Privileges->fetch_post_file($post_id);
		if($fetched_prev_data != null){
			foreach($fetched_prev_data as $data){
				$image_del[$counter] = $data['file_name'];
				$counter++;
			}
			for($i = 0; $i < $counter; $i++){
				unlink("./assets/images/post_images/".$image_del[$i]);
			}
			$this->Model_Privileges->delete_post_file($post_id);
		}
		$this->Model_Privileges->delete_post($post_id);
		echo "delete_success";
	}
	public function post_update_form_submit(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$this->form_validation->set_rules('title', '"Post Title"', 'required');
			$this->form_validation->set_rules('project_type', '"Project Type"', 'required');
			$this->form_validation->set_rules('desc', '"Description"', 'required');
			$this->form_validation->set_rules('land_desc', '"Land Description"', 'required');
			$this->form_validation->set_rules('exp_date', '"Expiration Date"', 'required');
			if($this->form_validation->run() == FALSE){
				$data['id'] = set_value('post_id'); 
			    $data['account_id'] = set_value('account_id');
			    $data['title'] = set_value('title');
			    $data['type'] = set_value('project_type');
			    $data['description'] = set_value('desc');
			    $data['land_description'] = set_value('land_desc');
			    $data['post_end'] = set_value('exp_date');
				$temp_data['data'] = $data;
				$this->load->view('templates/in/forms/post_forms/update_post', $temp_data);
			}
			else{
				$post_id = $this->input->post('post_id');
				$account_id = $this->input->post('account_id');
				$title = $this->input->post('title');
				$desc = $this->input->post('desc');
				$land_desc = $this->input->post('land_desc');
				$project_type = $this->input->post('project_type');
				$exp_date = $this->input->post('exp_date');
				$exp_time = date("H:i:s");
				$expiration = $exp_date." ".$exp_time;
				$cur_date = date("Y-m-d H:i:s");
				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('desc'),
					'account_id' => $account_id,
					'land_description' => $this->input->post('land_desc'),
					'type' => $project_type,
					'post_start' => $cur_date,
					'post_end' => $expiration,
					'status' => 'active');
				$this->Model_Privileges->update_post_client($insert_data, $post_id);
				if(!empty($_FILES)){
					$config['upload_path'] = "./assets/images/post_images";
					$config['allowed_types'] = 'jpg|png';
					$this->load->library('upload');

					$counter = 0;
					$fetched_prev_data = $this->Model_Privileges->fetch_post_file($post_id);
					if($fetched_prev_data != null){
						foreach($fetched_prev_data as $data){
							$image_del[$counter] = $data['file_name'];
							$counter++;
						}
						for($i = 0; $i < $counter; $i++){
							unlink("./assets/images/post_images/".$image_del[$i]);
						}
						$this->Model_Privileges->delete_post_file($post_id);
					}
					

					$files = $_FILES;
					$number_of_files = count($_FILES['file']['name']);
					$count = 0;
					for($i = 0; $i < $number_of_files; $i++){
						$_FILES['file']['name'] = $files['file']['name'][$i];
						$_FILES['file']['type'] = $files['file']['type'][$i];
						$_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];
						$_FILES['file']['size'] = $files['file']['size'][$i];
						$this->upload->initialize($config);
						if(!$this->upload->do_upload('file')){
							$error = array('error' => $this->upload->display_errors());
							echo $error['error'];
						}
						else{
							$final_files_data[] = $this->upload->data();
						}
					}	
					foreach($final_files_data as $file_data){
						$image_name[$count] = $file_data['file_name'];
						$count++;
					}
					for($i = 0; $i < $count; $i++){
						$post_data = array(
							'account_id' => $account_id,
							'post_id' => $post_id,
							'file_name' => $image_name[$i]);
					$this->db->insert('post_file',$post_data);
					}
				}
				echo "true";
			}

			// echo "<script>$('#news_body').load('refresh_news_page');</script>";
			// echo "<script>$('#timeline_page').load('refresh_timeline_page');</script>";
			// echo "<script>$('.close-modal').click();</script>";
			
		}
		else{
			echo "FUCK YOU! WAY NISULOD";
		}
	}

	public function cancel_application_response(){
		$post_id = $this->input->post('post_id');
		$this->Model_Privileges->cancel_application($post_id);
		echo "cancel_successful";
	}
}