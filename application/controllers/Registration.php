<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends MY_Controller {

	function client_registration_form_submit(){
		$this->form_validation->set_rules('username','USERNAME', 'required|callback_username_check');
		$this->form_validation->set_rules('password','PASSWORD', 'required|callback_password_check');
		$this->form_validation->set_rules('pass_conf','CONFIRM PASSWORD', 'required|matches[password]|callback_password_check');
		$this->form_validation->set_rules('email','EMAIL ADDRESS', 'required|valid_email|is_unique[user_account.email]|callback_email_check');
		$this->form_validation->set_rules('contact','CONTACT NUMBER', 'required|callback_contact_check');
		$this->form_validation->set_rules('first_name','FIRST NAME', 'required|callback_name_check');
		$this->form_validation->set_rules('last_name','LAST NAME', 'required|callback_name_check');
		$this->form_validation->set_rules('gender','GENDER', 'required');
		$this->form_validation->set_rules('birth_date','BIRTH DATE', 'required|callback_date_check');
		$this->form_validation->set_rules('occupation','OCCUPATION', 'required|callback_name_check');
		$this->form_validation->set_rules('work_address','WORK ADDRESS', 'required|callback_address_check');
		
		if ($this->form_validation->run() == FALSE){
			$this->load->view('templates/global_out/registration_page_client');
			$this->load->view('templates/assets/form_scripts/client_registration_script');
		}
		else{
			$username = $this->security->xss_clean($this->input->post('username'));
			$password = $this->security->xss_clean($this->input->post('password'));
			$email = $this->security->xss_clean($this->input->post('email'));
			$contact = $this->security->xss_clean($this->input->post('contact'));
			$fname = $this->security->xss_clean($this->input->post('first_name'));
			$lname = $this->security->xss_clean($this->input->post('last_name'));
			$gender = $this->security->xss_clean($this->input->post('gender'));
			$bdate = $this->security->xss_clean($this->input->post('birth_date'));
			$occupation = $this->security->xss_clean($this->input->post('occupation'));
			$work_address = $this->security->xss_clean($this->input->post('work_address'));

			$user_account = array(
				'username' => $username,
				'password' => $password,
				'email' => $email,
				'contact' => $contact,
				'profile_pic' => 'default.png',
				'privilege' => 'Client',
				'is_active' => 1
			);
			$account_id = $this->Model_Registrations->register_user_account($user_account);

			$user_profile = array(
				'account_id' => $account_id,
				'first_name' => $fname,
				'last_name' => $lname,
				'gender' => $gender,
				'birth_date' => $bdate);

			$this->Model_Registrations->register_user_profile($user_profile);

			$user_client = array(
				'account_id' => $account_id,
				'occupation' => $occupation,
				'work_address' => $work_address);
			$this->Model_Registrations->register_user_client($user_client);
			$full_name = $fname." ".$lname;
			$session_data = array(
				'username' => $username,
				'email' => $email,
				'contact' => $contact,
				'profile_pic' => 'default.png',
				'privilege' => 'Client',
				'full_name' => $full_name,
				'gender' => $gender,
				'birth_date' => $bdate,
				'occupation' => $occupation,
				'work_address' => $work_address
			);
			$this->session->set_userdata($session_data);
			echo "true";
			$dest = site_url('Account/landing_page');
			echo "<script>window.location.href='$dest'</script>";
		}
	}
	function builder_registration_form_submit(){
		$employer_fname = $this->input->post('employer_fname');
		$employer_lname = $this->input->post('employer_lname');
		$employer_fullname_first = $employer_fname." ".$employer_lname;
		$employer_fullname_second = $employer_lname. " ".$employer_fname;
		$building_type = $this->input->post('building_type');
		$location = $this->input->post('location');
		$engineer_name = $this->input->post('engineer_name');
		
		$result = $this->db->query("SELECT * FROM obo_records WHERE name_of_applicant LIKE 
		'$employer_fullname_first%' OR name_of_applicant LIKE '$employer_fullname_second%' AND 
		location LIKE '%$location%' AND building_type LIKE '$building_type%'")->result_array();
		var_dump($result);
	}
	public function admin_registration_form_submit(){
		$this->form_validation->set_rules('first_name','FIRST NAME', 'required|callback_name_check');
		$this->form_validation->set_rules('last_name','LAST NAME', 'required|callback_name_check');
		$this->form_validation->set_rules('gender','GENDER', 'required');
		$this->form_validation->set_rules('birth_date','BIRTH DATE', 'required|callback_date_check');
		$this->form_validation->set_rules('email','EMAIL ADDRESS', 'required|valid_email|is_unique[user_account.email]|callback_email_check');
		$this->form_validation->set_rules('contact','CONTACT NUMBER', 'required|callback_contact_check');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('templates/admin/admin/admin_registration_form');
		}
		else{
			$fname = $this->input->post('first_name');
			$lname = $this->input->post('last_name');
			$new_lname = str_replace(' ', '', $lname);
			$final_lname = strtolower($new_lname);
			$user_account = array(
					'username' => $final_lname,
					'password' => '12345',
					'email' => $this->input->post('email'),
					'contact' => $this->input->post('contact'),
					'privilege' => 'Admin',
					'is_active' => 1
			);
			$check_username = $this->Model_Registrations->checkUsername($user_account['username']);
			if($check_username){
				$previous_id= $this->Model_Registrations->getUserAccount();
				$new_id = intval($previous_id[0]['id']) + 1;
				$user_account['username'] = $final_lname.$new_id;
			}

			$account_id = $this->Model_Registrations->register_user_account($user_account);
			$user_profile = array(
				'account_id' => $account_id,
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'gender' => $this->input->post('gender'),
				'birth_date' => $this->input->post('birth_date')
			);
			$this->Model_Registrations->register_user_profile($user_profile);
			// echo "<script>$('#admin_modal .close').click();</script>";
			echo "success";
		}	
	}

	public function add_new_post_form_submit(){
			$this->form_validation->set_rules('title', '"Post Title"', 'required');
			$this->form_validation->set_rules('project_type', '"Project Type"', 'required');
			$this->form_validation->set_rules('desc', '"Description"', 'required');
			$this->form_validation->set_rules('land_desc', '"Land Description"', 'required');
			$this->form_validation->set_rules('exp_date', '"Expiration Date"', 'required');
			if($this->form_validation->run() == FALSE OR empty($_FILES)){
				if(empty($_FILES)){
					$temp_data['file_error'] = "Please upload an image of your land.";
					$this->load->view('templates/in/forms/post_forms/news_form', $temp_data);
				}
				else{
					$this->load->view('templates/in/forms/post_forms/news_form');
				}
				
			}
			else{
				$config['upload_path'] = "./assets/images/post_images";
				$config['allowed_types'] = 'jpg|png';
				$this->load->library('upload');

				$files = $_FILES;
				$number_of_files = count($_FILES['file']['name']);
				$count = 0;
				for($i = 0; $i < $number_of_files; $i++){
					$_FILES['file']['name'] = $files['file']['name'][$i];
					$_FILES['file']['type'] = $files['file']['type'][$i];
					$_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];
					$_FILES['file']['size'] = $files['file']['size'][$i];
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('file')){
						$error = array('error' => $this->upload->display_errors());
						echo $error['error'];
					}
					else{
						$final_files_data[] = $this->upload->data();
					}
				}	
				foreach($final_files_data as $file_data){
					$image_name[$count] = $file_data['file_name'];
					$count++;
				}

				$title = $this->input->post('title');
				$desc = $this->input->post('desc');
				$land_desc = $this->input->post('land_desc');
				$project_type = $this->input->post('project_type');
				$exp_date = $this->input->post('exp_date');
				$exp_time = date("H:i:s");
				$expiration = $exp_date." ".$exp_time;
				$cur_date = date("Y-m-d H:i:s");
				$data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('desc'),
					'land_description' => $this->input->post('land_desc'),
					'type' => $project_type,
					'post_start' => $cur_date,
					'post_end' => $expiration,
					'status' => 'active');
				$idents = $this->Model_Privileges->post_client($data);
				for($i = 0; $i < $count; $i++){
					$post_data = array(
						'account_id' => $idents['account_id'],
						'post_id' => $idents['post_id'],
						'file_name' => $image_name[$i]);
					$this->db->insert('post_file',$post_data);
				}
				echo "true";
				// echo "<script>$('#news_body').load('refresh_news_page');</script>";
			}
	}
	public function builder_request($client_id, $post_id){
		$email = $this->session->email;
		$data = $this->db->query("SELECT id FROM user_account WHERE email='$email'")->result_array();
		foreach($data as $data){
			$builder_id = $data['id'];
		}
		$data = $this->db->query("SELECT id FROM builder_post_request WHERE client_id = $client_id AND 
		post_id = $post_id AND builder_id = $builder_id")->result_array();
		if($data != null){

		}
		else{
			$search = $this->db->query("SELECT id FROM post_member WHERE client_id = $client_id AND 
			post_id = $post_id AND builder_id = $builder_id")->result_array();
			if($search != null){
				echo "false";
			}
			else{
				$insert_data = array(
				'client_id' => $client_id,
				'post_id' => $post_id,
				'builder_id' => $builder_id);
				$this->db->insert("builder_post_request", $insert_data);
				echo "true";
			}
		}
		
	}
	public function accept_post_application($post_id,$client_id,$builder_id, $id){
		$this->db->query("DELETE FROM builder_post_request WHERE id = $id");
		$array_insert = array(
			'post_id' => $post_id,
			'client_id' => $client_id,
			'builder_id' => $builder_id
			);
		$this->db->insert('post_member', $array_insert);
		echo "success";
	}
	
	public function submit_message(){
		// var_dump($_POST);
		$to = $this->input->post('to');
		$title = $this->input->post('title');
		$content = $this->input->post('content');

		$current_date = date('Y-m-d H:i:s');

		$from_fullname = $this->session->full_name;
		$sender_email = $this->session->email;
		$query = $this->db->query("SELECT id FROM user_account WHERE email = '$sender_email'")->result_array();
		foreach($query as $account_data){
			$sender_account_id = $account_data['id'];
		}
		$query_2 = $this->db->query("SELECT id FROM user_account WHERE email = '$to'")->result_array();
		foreach($query_2 as $account){
			$rec_account_id = $account['id'];
		}
		$query_3 = $this->db->query("SELECT first_name, last_name FROM user_profile WHERE account_id = 
		$rec_account_id")->result_array();
		foreach($query_3 as $acc){
			$to_fullname = $acc['first_name']." ".$acc['last_name'];
		}
		$insert = array(
			'account_from_id' => $sender_account_id,
			'from_name' => $from_fullname,
			'account_to_id' => $rec_account_id,
			'to_name' => $to_fullname,
			'title' => $title,
			'description' => $content,
			'date_sent' => $current_date);

		$this->db->insert('message', $insert);
		$insert_id = $this->db->insert_id();

		$arr['name'] = $this->session->full_name;
		$arr['email'] = $to;
		$arr['subject'] = $title;
		$arr['created_at'] = $current_date;
		$arr['id'] = $insert_id;
		$arr['new_count_message'] = 50;
		$arr['success'] = true;

		echo json_encode($arr);
	}

}


 ?>