<?php
class Account extends CI_Controller {

	public function landing_page(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/in/pages/landing';
			$this->load->view('templates/in/base_in', $temp_data);
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
		
	}
	public function profile_settings(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/in/pages/profile_settings';
			$this->load->view('templates/in/base_in', $temp_data);
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
	}



	public function timeline_page(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$temp_data['data'] = $this->Model_Privileges->fetch_post_account();
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/in/pages/timeline';
			$this->load->view('templates/in/base_in', $temp_data);
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
	}
	public function refresh_timeline_page(){
		if(isset($this->session->privilege)){
			$temp_data['data'] = $this->Model_Privileges->fetch_post_account();
			$this->load->view('templates/in/pages/timeline', $temp_data);
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}
	public function builder_timeline_page(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$temp_data['data'] = $this->Model_Privileges->fetch_application_account();
			$temp_data['proposal'] = $this->Model_Privileges->fetch_proposal_account();
			
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/in/pages/timeline_builder';
			$this->load->view('templates/in/base_in', $temp_data);
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
	}
	public function refresh_builder_timeline_page(){
		if(isset($this->session->privilege)){
			$temp_data['data'] = $this->Model_Privileges->fetch_application_account();
			$this->load->view('templates/in/pages/timeline_builder', $temp_data);
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}

	
	public function inbox_page(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$email = $this->session->email;
			$query = $this->db->query("SELECT id FROM user_account WHERE email = '$email'")->result_array();
			foreach($query as $account_data){
				$account_id = $account_data['id'];
			}
			$temp_data['message'] = $this->db->query("SELECT * FROM message WHERE account_to_id = $account_id")->result_array();

			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/in/pages/inbox';
			$this->load->view('templates/in/base_in', $temp_data);
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
	}
	public function news_page($page){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			$temp_data['data'] = $this->Model_Privileges->fetch_post_client();
			$temp_data['load_css'] = 'templates/assets/load_css';
			$temp_data['load_js'] = 'templates/assets/load_js'; 
			$temp_data['content'] = 'templates/in/pages/newsfeed';
			$this->load->view('templates/in/base_in', $temp_data);
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
	}
	public function refresh_news_page(){
		if(isset($this->session->privilege)){
			$temp_data['data'] = $this->Model_Privileges->fetch_post_client();
			$this->load->view('templates/in/pages/newsfeed', $temp_data);
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}
	// public function projects_page(){
	// 	$email = $this->session->email;
	// 	$data_account_fetched = $this->db->query("SELECT id FROM user_account WHERE email = '$email'")->result_array();
	// 	foreach($data_account_fetched as $account_data){
	// 		$account_id = $account_data['id'];
	// 	}

	// 	$temp_data['pending'] = $this->db->query("SELECT client_post.id, client_post.title, client_post.account_id, user_profile.first_name, user_profile.last_name FROM client_post, builder_post_request, user_profile WHERE client_post.id = builder_post_request.post_id AND client_post.account_id = builder_post_request.client_id AND user_profile.account_id = client_post.account_id AND builder_post_request.builder_id = $account_id")->result_array();
	// 	// $temp_data['data'] = $this->Model_Privileges->fetch_post_account();
	// 	$temp_data['load_css'] = 'templates/assets/load_css';
	// 	$temp_data['load_js'] = 'templates/assets/load_js'; 
	// 	$temp_data['content'] = 'templates/global_in/project_page';
	// 	$this->load->view('templates/global_in/base_body_in', $temp_data);
	// }
	 
	




	// PAGES THAT ONLY ADMIN CAN ACCESS
	public function admin_page(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				$temp_data['table_data'] = $this->Model_Accounts->select_all_admin();
				$temp_data['load_css'] = 'templates/assets/load_css';
				$temp_data['load_js'] = 'templates/assets/load_js'; 
				$temp_data['content'] = 'templates/admin/admin/admin_landing';
				$this->load->view('templates/in/base_in', $temp_data);
			}
			else{
				$dest = site_url('Account/landing_page');
				redirect($dest);
			}
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
	}
	public function refresh_admin_table(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				$temp_data['table_data'] = $this->Model_Accounts->select_all_admin();
				$this->load->view('templates/admin/admin/admin_landing', $temp_data);
			}
			else{
				$dest = site_url('Account/landing_page');
				echo "<script>window.location.href='$dest'</script>";
			}
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}
	public function client_page(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				$temp_data['table_data'] = $this->Model_Accounts->select_all_client();
				$temp_data['load_css'] = 'templates/assets/load_css';
				$temp_data['load_js'] = 'templates/assets/load_js'; 
				$temp_data['content'] = 'templates/admin/client/client_landing';
				$this->load->view('templates/in/base_in', $temp_data);
			}
			else{
				$dest = site_url('Account/landing_page');
				redirect($dest);
			}
		}
		else{
			$dest = site_url('UB/login');
			redirect($dest);
		}
	}
	public function refresh_client_table(){
		if(isset($this->session->username) AND isset($this->session->privilege)){
			if($this->session->privilege == 'Admin'){
				echo "<script>$('#client_modal .close-modal').click();</script>";
				echo "<script>$('#confirmation_modal .close').click();</script>";
				$temp_data['table_data'] = $this->Model_Accounts->select_all_client();
				$this->load->view('templates/admin/client/client_landing', $temp_data);
			}
			else{
				$dest = site_url('Account/landing_page');
				echo "<script>window.location.href='$dest'</script>";
			}
		}
		else{
			$dest = base_url();
			echo "<script>window.location.href='$dest'</script>";
		}
	}
}

